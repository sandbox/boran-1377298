Communitree & Communiverse
:::::::::::::::::::::::::: 

PURPOSE: 
The "Community Tree" displays most active Users and Nodes in a graphical form. When clicking on a user, lines are drawn to the nodes visited by the user. Node and user sizes depend on their popularity/activity.
The Tree consists of a Drupal services module (communiverse) that publishes user/nodes, and the javascript tool that pull node/user information and graphically shows it (communitree).

CONTRIBUTIONS / COMMUNITY:
communitree/communiverse for D7 is published as Opensource on drupal.org, the older versions are not published. License is GPL. Discussions: use the drupal issue queue fro this module or the communiverse.

HISTORY:
This version (communitree/communiverse) is a port/rewrite of "ctree2" by undef (http://www.undef.ch/) for the Swisscom Labs and the communiverse_service module for Drupal 6. 

::::::::::::::::::::::::::::::::::
The rest of this README concerns communitree only.
TODO: See XXX below

Configuration: 
--------------
Requires: Drupal 7, communiverse module (which publishes the node/user info we need)

Minimal config: 
- edit js/config.js
- visit index.html in this directory with your browser
- visit debug.html in this directory for a windows with aconole and debugging
- To put the tree into your own pages, look at the example communitree.html
  i.e. add a div called cTree cTreeRoot to an appropriate place in your page.
- flush caches
- enable the module

To understand the javascript sources, read js/README.txt.
If you make a js changes, aggregate js using aggreg.sh:
  cd js
  ./aggreg.sh
The live page should only loads aggreg.js, for performance reasons.


TODO: 
- abstract/config node/user settings and CSS class names.


Test/debugging:
--------------
Use firebug or your favourite inspector on:
http://YOURSERVER/MODULEPATH/communitree/index.html
http://YOURSERVER/sites/all/modules/communitree/index.html
e.g.
http://sislabsx.vptt.ch/sites/all/modules/sandbox/communitree/index.html

