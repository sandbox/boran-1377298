<?php
/** todo: doc
 */
$scale = $_GET['scale'];
$style = $_GET['style'];
if($scale == 0)
    $scale = 300;
$scaleHalf = $scale*.5;

function norm($value, $inputMin, $inputMax) {
	return (($value - $inputMin) / ($inputMax - $inputMin));
}

$radius = $scale/2;

$width =  $scale;
$height = $scale;
$border = 4;

$img = new Imagick();
$img->newImage( $width, $height, new ImagickPixel( 'transparent' ) );

$draw = new ImagickDraw();
$draw->setStrokeColor( new ImagickPixel( '#BBBBBB' ) );
$draw->setStrokeWidth( 1 );
$draw->setFillColor( new ImagickPixel( 'white' ) );

if($style=="hover"){
	$draw->setStrokeColor( new ImagickPixel( '#11AAFF' ) );
}

$num = 10;
$x = array();
$y = array();

$inx = array();
$iny = array();
//echo deg2rad(360)."<br />".(2*pi());

for($i = 0;$i<$num;$i++){
    $a = $i/$num*2*pi();
    $r = $radius*rand(50,100)/100;
        
    $x[] = sin($a)*$r+$scaleHalf;
    $y[] = cos($a)*$r+$scaleHalf;
    
    $inx[] = sin($a)*($r-$border)+$scaleHalf;
    $iny[] = cos($a)*($r-$border)+$scaleHalf;
    //$draw->rectangle($x[$i], $y[$i], $x[$i]+2, $y[$i]+2);
}

$points = array();
$points[] = array( 'x' => ($x[$num-1]+$x[0])/2, 'y' => ($y[$num-1]+$y[0])/2 );
for($i = 0;$i<$num-1;$i++){
        $points[] = array( 'x' => $x[$i], 'y' => $y[$i] );
        $points[] = array( 'x' => ($x[$i]+$x[$i+1])/2, 'y' => ($y[$i]+$y[$i+1])/2 ); 
}
$points[] = array( 'x' => $x[$num-1], 'y' => $y[$num-1] );
$points[] = array( 'x' => ($x[$num-1]+$x[0])/2, 'y' => ($y[$num-1]+$y[0])/2 ); 

$draw->bezier($points);

if($style=="hover"){
	$inpoints = array();
	$inpoints[] = array( 'x' => ($inx[$num-1]+$inx[0])/2, 'y' => ($iny[$num-1]+$iny[0])/2 );
	for($i = 0;$i<$num-1;$i++){
		$inpoints[] = array( 'x' => $inx[$i], 'y' => $iny[$i] );
		$inpoints[] = array( 'x' => ($inx[$i]+$inx[$i+1])/2, 'y' => ($iny[$i]+$iny[$i+1])/2 ); 
	}
	$inpoints[] = array( 'x' => $inx[$num-1], 'y' => $iny[$num-1] );
	$inpoints[] = array( 'x' => ($inx[$num-1]+$inx[0])/2, 'y' => ($iny[$num-1]+$iny[0])/2 );
	
	$draw->setFillColor( new ImagickPixel( '#11AAFF' ) );
	//$draw->setStrokeColor( new ImagickPixel( '#11AAFF' ) );
	$draw->bezier($inpoints);
}

$img->drawImage( $draw );
    
$img->setImageFormat( "png" );

header( "Content-Type: image/png" );
echo $img;
?>

