<?php
/** todo: doc
 */
header( "Content-Type: image/png" );

$scale = $_GET['scale'];
$style = $_GET['style'];
$area = $_GET['area'];


require_once "Cache/Lite/Output.php";

$options = array(
    'cacheDir' => '/tmp/',
    'lifeTime' => 7200,
    'pearErrorMode' => CACHE_LITE_ERROR_DIE
);

$cache = new Cache_Lite_Output($options);
$randID = rand(4,7);
if (!($cache->start($scale.";".$style.";".$area.";".$randID."; red"))) {

    if($scale == 0)
	$scale = 300;
    $scaleHalf = $scale*.5;
    
    $radius = $scale/2;
    
    $width =  $scale;
    $height = $scale;
    $border = 4;
    
    $img = new Imagick();
    $img->newImage( $width, $height, new ImagickPixel( 'transparent' ) );
    
    $draw = new ImagickDraw();
    
    $rb = 204;
    $gb = 238;
    $bb = 255;
    $randColb =  rand(0,50);
    $rb -= $randColb;
    $gb -= $randColb;
    $bb -= $randColb;
    
    $rr = 187;
    $gr = 187;
    $br = 187;
    $randColr =  rand(0,50);
    $rr += $randColr;
    $gr += $randColr;
    $br += $randColr;
    
    if($area == "u") $draw->setFillColor( new ImagickPixel( "rgb(". $rb .",". $gb .",". $bb .")" ) );
    else $draw->setFillColor( new ImagickPixel( "rgb(". $rr .",". $gr .",". $br .")" ) );
    if($style==red) $draw->setFillColor( new ImagickPixel( '#DD1122' ) );
    
    /*if($style=="hover"){
	    $draw->setStrokeColor( new ImagickPixel( '#11AAFF' ) );
    }
    
    if($style=="red"){
	    $draw->setStrokeColor( new ImagickPixel( '#DD1122' ) );
    }*/
    
    if($area=="help")$draw->setFillColor( new ImagickPixel( '#ededed' )) ;
    
    $draw->roundRectangle($border, $border, $scale-1-$border, $scale-1-$border, 10, 10);
    
    if($style=="hover"){// || $style=="red"){
	$draw->setFillColor( new ImagickPixel( '#11AAFF' ) );
	//if($style==red)  $draw->setFillColor( new ImagickPixel( '#DD1122' ) );
	if($scale>15){
	$draw->roundRectangle($border+4, $border+4, $scale-5-$border, $scale-5-$border, 8, 8);
	}else{
	$draw->roundRectangle($border, $border, $scale-1-$border, $scale-1-$border, 10, 10);  
	}
    }
    
    
    $img->drawImage( $draw );
    if($scale>40){
	$draw->setFont("../fonts/AndaleMono.ttf");
	//$draw->setFontSize($scale-$border*2);
	$draw->setFontSize(25);
	$draw->setGravity( Imagick::GRAVITY_CENTER );
	$randCol +=30;
	$draw->setFillColor(new ImagickPixel( "#11AAFF" ) );
	if($style=="hover") $draw->setFillColor(new ImagickPixel( "#ffffff" ) );
        
        
    if($area=="help"){
	    $img->annotateImage( $draw, 0, 0, 0, "?" );
    }
	
       /* if($area == "a")
	$img->annotateImage( $draw, 0, 0, 0, "A" );
	
	else if($area == "u")
	$img->annotateImage( $draw, 0, 0, 0, "U" );   
    
	else if($area == "t")
	$img->annotateImage( $draw, 0, 0, 0, "T" );
	
	else if($area == "n")
	$img->annotateImage( $draw, 0, 0, 0, "N" );*/
    }
    $img->setImageFormat( "png" );
    
    echo $img;

    // Cache not hit !
    // All the output is bufferised until the end() method
    // (...)

    $cache->end();

}
?>
