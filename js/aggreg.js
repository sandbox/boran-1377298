
function Logger(){var el;var elRoot;this.init=function(){$('body').append("<div id='logger'><div id='loggerContent'></div></div>");elRoot=$('#logger');elRoot.css("zIndex",99);el=$('#logger #loggerContent');this.toggle();}
this.log=function(m){el.append("> "+m+"<br />");}
this.toggle=function(){elRoot.toggle();}}
function dump(arr,level){var dumped_text="";if(!level)level=0;var level_padding="";for(var j=0;j<level+1;j++)level_padding+="    ";if(typeof(arr)=='object'){for(var item in arr){var value=arr[item];if(typeof(value)=='object'){dumped_text+=level_padding+"'"+item+"' ...\n";}else{dumped_text+=level_padding+"'"+item+"' => \""+value+"\"\n";}}}else{dumped_text="===>"+arr+"<===("+typeof(arr)+")";}
return dumped_text;}
var contentIsShown="yes";function showCommunitree(){document.getElementById("cTreeRoot").style.zIndex="10000";document.getElementById("cTreeRoot").style.display="block";if(document.getElementById("zone-postscript-wrapper")!=null)
document.getElementById.style.display="none";if(document.getElementById("region-sidebar-second")!=null)
document.getElementById("region-sidebar-second").style.display="none";if(document.getElementById("zone-footer-wrapper")!=null)
document.getElementById("zone-footer-wrapper").style.display="none";$("#cTreeToogleButton").addClass("closeButton");contentIsShown="no";}
function hideCommunitree(){document.getElementById("cTreeRoot").style.display="none";document.getElementById("cTreeRoot").style.zIndex="-10000";if(document.getElementById("zone-postscript-wrapper")!=null)
document.getElementById("zone-postscript-wrapper").style.display="block";if(document.getElementById("region-sidebar-second")!=null)
document.getElementById("region-sidebar-second").style.display="block";if(document.getElementById("zone-footer-wrapper")!=null)
document.getElementById("zone-footer-wrapper").style.display="block";$("#cTreeToogleButton").removeClass("closeButton");contentIsShown="yes";}
function toggleTree(){if(contentIsShown=="yes"){showCommunitree();}
else{hideCommunitree();}}
function map(value,istart,istop,ostart,ostop){return ostart+(ostop-ostart)*((value-istart)/(istop-istart));}
function randomf(a,b){var d=b-a;return Math.random()*d+a;}
function randomi(a,b){return Math.floor(randomf(a,b+1));}
function random1i(){if(Math.random()>.5)
return-1;else
return 1;}
var gridSizeX=7;var gridSizeY=gridSizeX;function toGridX(a){return Math.floor(a/gridSizeX)*gridSizeX;}
function toGridY(a){return toGridX(a);}
var fieldID=99999;function nextFieldID(){return fieldID++;}
var lineID=0;function nextLineID(){return lineID++;}
var travellerID=0;function nextTravellerID(){return travellerID++;}
var connectionID=0;function nextConnectionID(){return connectionID++;}
var areaID=0;function nextAreaID(){return areaID++;}
function isInRect(x,y,rx,ry,w,h){if(x<=rx)return false;if(x>=rx+w)return false;if(y<=ry)return false;if(y>=ry+h)return false;return true;}
function Point(_x,_y){this.x=_x;this.y=_y;this.toJsPoint=function(){return new jsPoint(this.x,this.y);}}
function Rect(_x,_y,_w,_h){this.x=_x;this.y=_y;this.width=_w;this.height=_h;this.set=function(x,y,w,h){this.x=x;this.y=y;this.width=w;this.height=h;}
this.intersects=function(r){if(this.bottom()<=r.top())return false;if(this.top()>=r.bottom())return false;if(this.right()<=r.left())return false;if(this.left()>=r.right())return false;return true;};this.intersectsWithSpace=function(r){if(this.bottom()+gridSizeY<=r.top())return false;if(this.top()-gridSizeY>=r.bottom())return false;if(this.right()+gridSizeX<=r.left())return false;if(this.left()-gridSizeX>=r.right())return false;return true;};this.contains=function(x,y){return isInRect(x,y,this.x,this.y,this.width,this.height);}
this.top=function(){return this.y;};this.bottom=function(){return this.y+this.height;};this.left=function(){return this.x;}
this.right=function(){return this.x+this.width;}
this.centerXf=function(){return this.x+this.width*.5;}
this.centerYf=function(){return this.y+this.height*.5;}
this.centerXi=function(){return this.x+Math.round(this.width*.5);}
this.centerYi=function(){return this.y+Math.round(this.height*.5);}
this.centeri=function(){return new Point(this.centerXi(),this.centerYi());}
this.centerf=function(){return new Point(this.centerXf(),this.centerYf());}}
function linkDescr(title,url){this.title=title;this.url=url;}
function easeOut(t,b,c,d){return-c*((t=t/d-1)*t*t*t-1)+b;}
function mergeJSON(obja,objb){var i=0;for(var z in objb){if(objb.hasOwnProperty(z)){obja[z]=objb[z];}}
return obja;}
function print_r(theObj,indent){var output="";if(indent==undefined){indent="  ";}else{indent+="  ";}
if(theObj.constructor==Array||theObj.constructor==Object){for(var p in theObj){if(theObj[p].constructor==Array||theObj[p].constructor==Object){var type=(theObj[p].constructor==Array)?"Array":"Object";output+=indent+"["+p+"]("+type+")=>\n";output+=print_r(theObj[p],indent);}else{output+=indent+"["+p+"]:"+theObj[p]+"\n";}}}
return output;}
function deselect()
{if(document.selection)
document.selection.empty();else if(window.getSelection)
window.getSelection().removeAllRanges();}
function clickMe(){this.element;this.hidden=false;this.draw=function(){this.element=$('#ctClick');this.hide();}
this.hide=function(){}
this.show=function(){this.elememt.show();this.hidden=false;}
this.setPos=function(x,y){this.element.css("left",(x+12)+"px");this.element.css("top",(y-5)+"px");}
this.setPosY=function(y){this.element.css("top",(y-5)+"px");}
this.setInner=function(s){this.element.html(s);}};var hexcase=0;var b64pad="";function hex_md5(s){return rstr2hex(rstr_md5(str2rstr_utf8(s)));}
function b64_md5(s){return rstr2b64(rstr_md5(str2rstr_utf8(s)));}
function any_md5(s,e){return rstr2any(rstr_md5(str2rstr_utf8(s)),e);}
function hex_hmac_md5(k,d)
{return rstr2hex(rstr_hmac_md5(str2rstr_utf8(k),str2rstr_utf8(d)));}
function b64_hmac_md5(k,d)
{return rstr2b64(rstr_hmac_md5(str2rstr_utf8(k),str2rstr_utf8(d)));}
function any_hmac_md5(k,d,e)
{return rstr2any(rstr_hmac_md5(str2rstr_utf8(k),str2rstr_utf8(d)),e);}
function md5_vm_test()
{return hex_md5("abc").toLowerCase()=="900150983cd24fb0d6963f7d28e17f72";}
function rstr_md5(s)
{return binl2rstr(binl_md5(rstr2binl(s),s.length*8));}
function rstr_hmac_md5(key,data)
{var bkey=rstr2binl(key);if(bkey.length>16)bkey=binl_md5(bkey,key.length*8);var ipad=Array(16),opad=Array(16);for(var i=0;i<16;i++)
{ipad[i]=bkey[i]^0x36363636;opad[i]=bkey[i]^0x5C5C5C5C;}
var hash=binl_md5(ipad.concat(rstr2binl(data)),512+data.length*8);return binl2rstr(binl_md5(opad.concat(hash),512+128));}
function rstr2hex(input)
{try{hexcase}catch(e){hexcase=0;}
var hex_tab=hexcase?"0123456789ABCDEF":"0123456789abcdef";var output="";var x;for(var i=0;i<input.length;i++)
{x=input.charCodeAt(i);output+=hex_tab.charAt((x>>>4)&0x0F)
+hex_tab.charAt(x&0x0F);}
return output;}
function rstr2b64(input)
{try{b64pad}catch(e){b64pad='';}
var tab="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";var output="";var len=input.length;for(var i=0;i<len;i+=3)
{var triplet=(input.charCodeAt(i)<<16)|(i+1<len?input.charCodeAt(i+1)<<8:0)|(i+2<len?input.charCodeAt(i+2):0);for(var j=0;j<4;j++)
{if(i*8+j*6>input.length*8)output+=b64pad;else output+=tab.charAt((triplet>>>6*(3-j))&0x3F);}}
return output;}
function rstr2any(input,encoding)
{var divisor=encoding.length;var i,j,q,x,quotient;var dividend=Array(Math.ceil(input.length/2));for(i=0;i<dividend.length;i++)
{dividend[i]=(input.charCodeAt(i*2)<<8)|input.charCodeAt(i*2+1);}
var full_length=Math.ceil(input.length*8/(Math.log(encoding.length)/Math.log(2)));var remainders=Array(full_length);for(j=0;j<full_length;j++)
{quotient=Array();x=0;for(i=0;i<dividend.length;i++)
{x=(x<<16)+dividend[i];q=Math.floor(x/divisor);x-=q*divisor;if(quotient.length>0||q>0)
quotient[quotient.length]=q;}
remainders[j]=x;dividend=quotient;}
var output="";for(i=remainders.length-1;i>=0;i--)
output+=encoding.charAt(remainders[i]);return output;}
function str2rstr_utf8(input)
{var output="";var i=-1;var x,y;while(++i<input.length)
{x=input.charCodeAt(i);y=i+1<input.length?input.charCodeAt(i+1):0;if(0xD800<=x&&x<=0xDBFF&&0xDC00<=y&&y<=0xDFFF)
{x=0x10000+((x&0x03FF)<<10)+(y&0x03FF);i++;}
if(x<=0x7F)
output+=String.fromCharCode(x);else if(x<=0x7FF)
output+=String.fromCharCode(0xC0|((x>>>6)&0x1F),0x80|(x&0x3F));else if(x<=0xFFFF)
output+=String.fromCharCode(0xE0|((x>>>12)&0x0F),0x80|((x>>>6)&0x3F),0x80|(x&0x3F));else if(x<=0x1FFFFF)
output+=String.fromCharCode(0xF0|((x>>>18)&0x07),0x80|((x>>>12)&0x3F),0x80|((x>>>6)&0x3F),0x80|(x&0x3F));}
return output;}
function str2rstr_utf16le(input)
{var output="";for(var i=0;i<input.length;i++)
output+=String.fromCharCode(input.charCodeAt(i)&0xFF,(input.charCodeAt(i)>>>8)&0xFF);return output;}
function str2rstr_utf16be(input)
{var output="";for(var i=0;i<input.length;i++)
output+=String.fromCharCode((input.charCodeAt(i)>>>8)&0xFF,input.charCodeAt(i)&0xFF);return output;}
function rstr2binl(input)
{var output=Array(input.length>>2);for(var i=0;i<output.length;i++)
output[i]=0;for(var i=0;i<input.length*8;i+=8)
output[i>>5]|=(input.charCodeAt(i/8)&0xFF)<<(i%32);return output;}
function binl2rstr(input)
{var output="";for(var i=0;i<input.length*32;i+=8)
output+=String.fromCharCode((input[i>>5]>>>(i%32))&0xFF);return output;}
function binl_md5(x,len)
{x[len>>5]|=0x80<<((len)%32);x[(((len+64)>>>9)<<4)+14]=len;var a=1732584193;var b=-271733879;var c=-1732584194;var d=271733878;for(var i=0;i<x.length;i+=16)
{var olda=a;var oldb=b;var oldc=c;var oldd=d;a=md5_ff(a,b,c,d,x[i+0],7,-680876936);d=md5_ff(d,a,b,c,x[i+1],12,-389564586);c=md5_ff(c,d,a,b,x[i+2],17,606105819);b=md5_ff(b,c,d,a,x[i+3],22,-1044525330);a=md5_ff(a,b,c,d,x[i+4],7,-176418897);d=md5_ff(d,a,b,c,x[i+5],12,1200080426);c=md5_ff(c,d,a,b,x[i+6],17,-1473231341);b=md5_ff(b,c,d,a,x[i+7],22,-45705983);a=md5_ff(a,b,c,d,x[i+8],7,1770035416);d=md5_ff(d,a,b,c,x[i+9],12,-1958414417);c=md5_ff(c,d,a,b,x[i+10],17,-42063);b=md5_ff(b,c,d,a,x[i+11],22,-1990404162);a=md5_ff(a,b,c,d,x[i+12],7,1804603682);d=md5_ff(d,a,b,c,x[i+13],12,-40341101);c=md5_ff(c,d,a,b,x[i+14],17,-1502002290);b=md5_ff(b,c,d,a,x[i+15],22,1236535329);a=md5_gg(a,b,c,d,x[i+1],5,-165796510);d=md5_gg(d,a,b,c,x[i+6],9,-1069501632);c=md5_gg(c,d,a,b,x[i+11],14,643717713);b=md5_gg(b,c,d,a,x[i+0],20,-373897302);a=md5_gg(a,b,c,d,x[i+5],5,-701558691);d=md5_gg(d,a,b,c,x[i+10],9,38016083);c=md5_gg(c,d,a,b,x[i+15],14,-660478335);b=md5_gg(b,c,d,a,x[i+4],20,-405537848);a=md5_gg(a,b,c,d,x[i+9],5,568446438);d=md5_gg(d,a,b,c,x[i+14],9,-1019803690);c=md5_gg(c,d,a,b,x[i+3],14,-187363961);b=md5_gg(b,c,d,a,x[i+8],20,1163531501);a=md5_gg(a,b,c,d,x[i+13],5,-1444681467);d=md5_gg(d,a,b,c,x[i+2],9,-51403784);c=md5_gg(c,d,a,b,x[i+7],14,1735328473);b=md5_gg(b,c,d,a,x[i+12],20,-1926607734);a=md5_hh(a,b,c,d,x[i+5],4,-378558);d=md5_hh(d,a,b,c,x[i+8],11,-2022574463);c=md5_hh(c,d,a,b,x[i+11],16,1839030562);b=md5_hh(b,c,d,a,x[i+14],23,-35309556);a=md5_hh(a,b,c,d,x[i+1],4,-1530992060);d=md5_hh(d,a,b,c,x[i+4],11,1272893353);c=md5_hh(c,d,a,b,x[i+7],16,-155497632);b=md5_hh(b,c,d,a,x[i+10],23,-1094730640);a=md5_hh(a,b,c,d,x[i+13],4,681279174);d=md5_hh(d,a,b,c,x[i+0],11,-358537222);c=md5_hh(c,d,a,b,x[i+3],16,-722521979);b=md5_hh(b,c,d,a,x[i+6],23,76029189);a=md5_hh(a,b,c,d,x[i+9],4,-640364487);d=md5_hh(d,a,b,c,x[i+12],11,-421815835);c=md5_hh(c,d,a,b,x[i+15],16,530742520);b=md5_hh(b,c,d,a,x[i+2],23,-995338651);a=md5_ii(a,b,c,d,x[i+0],6,-198630844);d=md5_ii(d,a,b,c,x[i+7],10,1126891415);c=md5_ii(c,d,a,b,x[i+14],15,-1416354905);b=md5_ii(b,c,d,a,x[i+5],21,-57434055);a=md5_ii(a,b,c,d,x[i+12],6,1700485571);d=md5_ii(d,a,b,c,x[i+3],10,-1894986606);c=md5_ii(c,d,a,b,x[i+10],15,-1051523);b=md5_ii(b,c,d,a,x[i+1],21,-2054922799);a=md5_ii(a,b,c,d,x[i+8],6,1873313359);d=md5_ii(d,a,b,c,x[i+15],10,-30611744);c=md5_ii(c,d,a,b,x[i+6],15,-1560198380);b=md5_ii(b,c,d,a,x[i+13],21,1309151649);a=md5_ii(a,b,c,d,x[i+4],6,-145523070);d=md5_ii(d,a,b,c,x[i+11],10,-1120210379);c=md5_ii(c,d,a,b,x[i+2],15,718787259);b=md5_ii(b,c,d,a,x[i+9],21,-343485551);a=safe_add(a,olda);b=safe_add(b,oldb);c=safe_add(c,oldc);d=safe_add(d,oldd);}
return Array(a,b,c,d);}
function md5_cmn(q,a,b,x,s,t)
{return safe_add(bit_rol(safe_add(safe_add(a,q),safe_add(x,t)),s),b);}
function md5_ff(a,b,c,d,x,s,t)
{return md5_cmn((b&c)|((~b)&d),a,b,x,s,t);}
function md5_gg(a,b,c,d,x,s,t)
{return md5_cmn((b&d)|(c&(~d)),a,b,x,s,t);}
function md5_hh(a,b,c,d,x,s,t)
{return md5_cmn(b^c^d,a,b,x,s,t);}
function md5_ii(a,b,c,d,x,s,t)
{return md5_cmn(c^(b|(~d)),a,b,x,s,t);}
function safe_add(x,y)
{var lsw=(x&0xFFFF)+(y&0xFFFF);var msw=(x>>16)+(y>>16)+(lsw>>16);return(msw<<16)|(lsw&0xFFFF);}
function bit_rol(num,cnt)
{return(num<<cnt)|(num>>>(32-cnt));}
function Distributable(){this.parent=null;this.draw=function(){};this.hide=function(){};this.absPos=function(){var p=new Point(this.x,this.y);var cur=this;while(cur.parent!=null){p.x+=cur.parent.x;p.y+=cur.parent.y;cur=cur.parent;}
return p;}}
Distributable.prototype=new Rect(0,0,0,0);function Distributor(){this.children=[];this.childrenFit=[];this.deadZones=[];this.lastFit=0;this.alert=function(){alert("OK");}
this.addChild=function(r){r.parent=this;this.children.push(r);}
this.addChildFit=function(r){this.childrenFit.push(r);}
this.childAt=function(id){return this.childrenFit[id];}
this.hasChildAt=function(x,y){for(var i=0;i<this.childrenFit.length;i++){if(this.childrenFit[i].contains(x,y))
return i;}
return-1;}
this.hasChildrenAt=function(x,y){var ret=[];for(var i=0;i<this.childrenFit.length;i++){if(this.childrenFit[i].contains(x,y))
ret.push(i);}
return ret;}
this.addDeadZone=function(x,y,w,h){var d=new Rect(x,y,w,h);this.deadZones.push(d);}
this.intersectsDeadZone=function(r){for(var j=0;j<this.deadZones.length;j++){if(r.intersects(this.deadZones[j])){return true;}}
return false;}
this.processChildren=function(){this.arrangeChildren();}
this.arrangeChildren=function(){var dropped=0;for(var i=0;i<this.children.length;i++){var d=this.children[i];if(!this.fitChild(d)){dropped++;}}
log('distributor.arrangeChildren '+this.name+' '
+this.children.length+" Dropped "+dropped);}
getSpiralPos=function(cx,cy,radius,windings,imax,i){var offset=radius/(windings*360);var angle=map(i,0,imax,0,360*windings);var currentRadius=angle*offset;var x=cx+currentRadius*Math.cos(angle);var y=cy+currentRadius*Math.sin(angle);return new Point(x,y);}
this.fitChild=function(d){var fits=false;var count=0;var maxCount=80;if($.browser.msie==true){maxCount=20;}
while(fits==false&&count<maxCount){var mW;if(this.width>this.height)mW=this.height;else mW=this.width;var spiralPos=getSpiralPos(this.width/2,this.height/2,mW/2,randomi(3,33),maxCount,count);d.x=toGridX(spiralPos.x);d.y=toGridY(spiralPos.y);if(d.x>this.width-d.width||d.y>this.height-d.height){fits=false;}else{var sects=false;if(this.intersectsDeadZone(d)==false){for(var j=0;j<this.childrenFit.length;j++){if(this.childrenFit[j]!=d)
if(d.intersectsWithSpace(this.childrenFit[j]))
sects=true;}}else{sects=true;}
fits=!sects;}
count++;}
if(!fits){d.hide();return false;}else{d.draw();this.childrenFit.push(d);return true;}
return false;}
this.randomChild=function(){return this.childrenFit[randomi(0,this.childrenFit.length-1)];}
this.forceFitField=function(f){var fits=false;var count=0;var maxCount=70;while(fits==false&&count<maxCount){var sects=false;f.x=toGridX(randomi(0,this.width-f.width));f.y=toGridY(randomi(0,this.height-f.height));if(this.intersectsDeadZone(f)==false){for(var j=0;j<this.childrenFit.length;j++){if(this.childrenFit[j]!=f)
if(f.intersectsWithSpace(this.childrenFit[j]))
sects=true;}}else{sects=true;}
fits=!sects;count++;}
if(!fits){for(var j=0;j<this.childrenFit.length;j++){var c=this.childrenFit[j];if(c.width>=f.width){c.element.fadeOut();this.childrenFit.splice(j,1);this.childrenFit.push(f);f.x=c.x;f.y=c.y;fits=true;break;}}}
if(fits){f.draw();f.hide();f.fadeIn();this.childrenFit.push(f);}
return fits;}
this.getVisibleByID=function(id){for(var j=0;j<this.childrenFit.length;j++){if(this.childrenFit[j].id==id)
return this.childrenFit[j];}
return undefined;}
this.getByID=function(id){for(var j=0;j<this.children.length;j++){if(this.children[j].id==id)
return this.children[j];}
return undefined;}}
Distributor.prototype=new Distributable(0,0,0,0);function Area(){this.name="NO NAME";this.element;this.areaNameDiv;this.cssClass="emptyArea";this.id=0;this.children=[];this.childrenFit=[];this.deadZones=[];this.isArea=true;this.minSize=0;this.maxSize=1;this.diffSize=1;this.mult=1;this.ids=new Array();this.init=function(parent){this.id=nextAreaID();parent.element.append("<div id='area"+this.id+"' class='area "+this.cssClass+"'></div>");this.element=$("#area"+this.id);this.element.append("<div id='areaNameDiv"+this.id+"' class='areaNameDiv'><div id='blueArrow'></div><h1>"+this.name+"</h1><h2>"+this.width+"</h2></div>");this.areaNameDiv=$("#areaNameDiv"+this.id);}
this.draw=function(){this.element=$("#area"+this.id);this.element.width(this.width);this.element.height(this.height);this.element.css("left",this.x);this.element.css("top",this.y);}
this.hasID=function(id){for(var i=0;i<this.ids.length;i++){if(this.ids[i]==id)
return true;}
return false;}
this.fillDummyData=function(){for(var i=0;i<20;i++){var f=new Field();f.id=nextFieldID();var s=randomi(30,80);f.width=toGridX(s);f.height=toGridY(s);f.init(this);this.addChild(f);}
this.processChildren();}
this.growAllChildren=function(){for(var i=0;i<this.childrenFit.length;i++){this.childrenFit[i].grow();}}
this.fadeInAllChildren=function(){for(var i=0;i<this.childrenFit.length;i++){this.childrenFit[i].fadeIn();}}
this.hideChildren=function(howMany){var amt=Math.round(this.childrenFit.length*howMany);for(var i=0;i<amt;i++){this.childrenFit[i].hide();}}
this.hideAllChildren=function(){for(var i=0;i<this.childrenFit.length;i++){this.childrenFit[i].hide();}}
this.showAllChildren=function(){for(var i=0;i<this.childrenFit.length;i++){this.childrenFit[i].show();}}}
Area.prototype=new Distributor();function Field(){this.name="No Name";this.element;this.inRect;this.cssClass="fieldBlank";this.isArea=false;this.area="";var offset=2;this.visible=true;this.isActive=false;this.init=function(parent){parent.element.append("<div id='field"+this.area+this.id+"' class='field "+this.cssClass+"' ></div>");this.element=$("#field"+this.area+this.id);var ref=this;this.element.hover(function(){ref.onMouseOver()},function(){ref.onMouseOut()});this.element.click(function(){ref.onClick()});this.defaultBG=httpRoot+"images/php/roundCorner.php?scale="+this.width+"&area="+this.area;this.hoverBG=httpRoot+"images/php/roundCorner.php?scale="+this.width+"&style=hover&area="+this.area;this.activeBG=httpRoot+"images/php/roundCorner.php?scale="+this.width+"&style=hover&area="+this.area;this.onInit();}
this.onInit=function(){}
this.onMouseOver=function(){this.element.addClass('fieldOver');if($.browser.msie==true)
this.element.css('filter',"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+this.hoverBG+"', sizingMethod='scale')");else
this.element.css("background-image","url("+this.hoverBG+")");info.setData(this.name,this.getInfoData());info.setImage(this.getImageData());info.moveTo(this.x+this.parent.x+this.width/2,this.y+this.parent.y+this.height,this.height);info.show();this.drawActivity();}
this.getImageData=function(){return undefined;}
this.getInfoData=function(){return["Id: "+this.id,"Width: "+this.width,"Pos: "+this.x+"x /"+this.y+"y"];}
this.onMouseOut=function(){info.hide();this.element.removeClass('fieldOver');if(this.isActive==false){if($.browser.msie==true)
this.element.css('filter',"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+this.defaultBG+"', sizingMethod='scale')");else
this.element.css("background-image","url("+this.defaultBG+")");}}
this.onClick=function(){}
this.grow=function(){this.element.width(0);this.element.height(0);this.element.css("left",this.x+offset);this.element.css("top",this.y+offset);this.element.animate({width:this.width-1-offset*2,height:this.height-1-offset*2,left:this.x+offset,top:this.y+offset},800,function(){});}
this.fadeIn=function(){var ref=this;this.element.fadeIn(1400,function(){ref.show()});}
this.fadeOut=function(){var ref=this;this.element.fadeOut(1100,function(){ref.hide()});}
this.hide=function(){this.visible=false;this.element.hide();}
this.show=function(){this.visible=true;this.element.show();}
this.activate=function(){this.isActive=true;this.element.addClass('fieldActive');if($.browser.msie==true)
this.element.css('filter',"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+this.activeBG+"', sizingMethod='scale')");else
this.element.css("background-image","url("+this.activeBG+")");}
this.deactivate=function(){this.isActive=false;this.element.removeClass('fieldActive');if($.browser.msie==true)
this.element.css('filter',"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+this.defaultBG+"', sizingMethod='scale')");else
this.element.css("background-image","url("+this.defaultBG+")");}
this.draw=function(){this.element.width(this.width);this.element.height(this.height);this.element.css("left",this.x);this.element.css("top",this.y);if($.browser.msie==true)
this.element.css('filter',"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+this.defaultBG+"', sizingMethod='scale')");else
this.element.css("background-image","url("+this.defaultBG+")");}
this.getCenter=function(){var ret=new Point();ret.x=this.centerXi()+this.parent.x;ret.y=this.centerYi()+this.parent.y;return ret;}
this.getConnectionStart=function(p){var ret=new Point();var dX=p.x-this.x;var dY=p.y-this.y;if(Math.abs(dX)<Math.abs(dY)){if(dX<0)
ret.x=this.x;else
ret.x=this.right();ret.y=toGridY(this.y+this.height*.5);}else{if(dY>0)
ret.y=this.y;else
ret.y=this.bottom();ret.x=toGridX(this.x+this.width*.5);}
ret.x+=this.parent.x;ret.y+=this.parent.y;return ret;}
this.hide=function(){this.element.hide();}}
Field.prototype=new Distributable();function Connection(){this.a;this.b;this.root;this.curPos;this.points=[];this.id=nextConnectionID();this.passThru=[];lineElement.append("<div id='connection"+this.id+"' class='connection'></div>");this.element=$("#connection"+this.id);this.init=function(a,b){this.a=a;this.b=b;this.curPos=this.a;if(a.x==b.x&&a.y==b.y)
return;this.process();}
this.addPassThru=function(p){this.passThru.push(p);}
this.isPassThru=function(p){for(var i=0;i<this.passThru.length;i++){if(this.passThru[i]==p)
return true;}
return false;}
this.pointAt=function(i){return this.points[i];}
this.pointsAmount=function(){return this.points.length;}
this.addPointEnd=function(p){this.points.push(p);}
this.addPointBeginning=function(p){this.points.splice(0,0,p);}
this.kill=function(){this.element.html("");}
this.process=function(){this.points=[];var i=0;var preferX=true;do{var oldPos=new Point(this.curPos.x,this.curPos.y);this.points.push(new Point(this.curPos.x,this.curPos.y));var dX=this.b.x-this.curPos.x;var xDir=0;if(dX!=0)
xDir=dX/Math.abs(dX);var dY=this.b.y-this.curPos.y;var yDir=0;if(dY!=0)
yDir=dY/Math.abs(dY);var moveX=true;if(this.curPos.x==this.b.x)
moveX=false;if(moveX)
this.curPos.x+=xDir*gridSizeX;else
this.curPos.y+=yDir*gridSizeY;var ids=master.hasChildrenAt(this.curPos.x,this.curPos.y);if(ids.length>0){for(var ii=0;ii<ids.length;ii++){var distMaster=master.childAt(ids[ii]);var id=distMaster.hasChildAt(this.curPos.x-distMaster.x,this.curPos.y-distMaster.y);if(id!=-1){var dist=distMaster.childAt(id);this.curPos=oldPos;if(moveX){var j=0;if(yDir==0){if(Math.abs(this.curPos.y-dist.y)<Math.abs(this.curPos.y-dist.bottom()))
yDir=1;else
yDir=-1;}
while(this.curPos.y-distMaster.y>dist.y&&this.curPos.y-distMaster.y<dist.bottom()){this.curPos.y+=yDir*gridSizeY;this.points.push(new Point(this.curPos.x,this.curPos.y));j++;if(j>200){log("had to break while x avoiding obstacle");break;}}
this.curPos.x+=xDir*gridSizeX;}else{if(xDir==0){if(Math.abs(this.curPos.x-dist.x)<Math.abs(this.curPos.x-dist.right()))
xDir=1;else
xDir=-1;}
var j=0;while(this.curPos.x-distMaster.x>dist.x&&this.curPos.x-distMaster.x<dist.right()){this.curPos.x+=xDir*gridSizeX;this.points.push(new Point(this.curPos.x,this.curPos.y));j++;if(j>200){log("had to break while y avoiding obstacle");break;}}
this.curPos.y+=yDir*gridSizeY;}}}}
i++;if(i>200){log("had to break pathfinder while moving to target, i got over a thousand! xDir:"+xDir+" moveX:"+moveX+" yDir:"+yDir);break;}}while(this.curPos.x!=this.b.x||this.curPos.y!=this.b.y);this.points.push(this.b);}
this.draw=function(){var cX=this.points[0].x;var cY=this.points[0].y;for(var i=1;i<this.pointsAmount();i++){this.drawStep(i);}}
this.drawStep=function(step){var a=this.pointAt(step-1);var b=this.pointAt(step);var w=Math.abs(a.x-b.x);var h=Math.abs(a.y-b.y);var x=a.x;if(b.x<a.x)
x=b.x;var y=a.y;if(b.y<a.y)
y=b.y;var cl="";if(w==0)
w=1;if(h==0)
h=1;if(w<h)
cl="h";else
cl="w";this.element.append("<div class='"+cl+"' style='width:"+w+"px;height:"+h+"px;top:"+y+"px;left:"+x+"px'></div>");}}var travellers=[];function killTravellers(){for(var i=0;i<travellers.length;i++){travellers[i].killFade();}
travellers=[];}
function Traveller(){var connection=null;var pos=0;var interpol=0;this.element=null;this.width=9;this.height=9;this.widthHalf=4;this.heightHalf=4;this.parent=lineElement;this.id=nextTravellerID();this.parent.append("<div id='traveller"+this.id+"' class='traveller'>&nbsp;</div>");this.element=$("#traveller"+this.id);this.element.width(this.width);this.element.height(this.height);this.fieldA=null;this.fieldB=null;this.speed=.55;this.intervalID=0;this.dir=-1;this.oldDir=-1;travellers.push(this);this.setConnection=function(con){this.connection=con;this.go();}
this.setFields=function(a,b){if(a==undefined||b==undefined)
return;this.fieldA=a;this.fieldB=b;a.activate();var con=new Connection();var aStart=a.getConnectionStart(b);var bStart=b.getConnectionStart(a);con.init(aStart,bStart);con.addPointBeginning(a.getCenter());con.addPointEnd(b.getCenter());this.setConnection(con);}
this.getDirection=function(a,b){this.oldDir=this.dir;this.dir=getDirection(a,b);this.updateBG();}
this.go=function(){var theRef=this;var speed=30;if($.browser.msie==true){speed=30;}
this.intervalID=setInterval(function(){theRef.process()},speed);this.pos=1;this.interpol=0;this.getDirection(this.connection.pointAt(0),this.connection.pointAt(1));}
this.process=function(){var a=this.connection.pointAt(this.pos);var b=this.connection.pointAt(this.pos-1);this.x=a.x;this.y=b.y;this.draw();this.getDirection(b,a);this.interpol+=this.speed;if(this.interpol>1){this.connection.drawStep(this.pos);this.pos++;this.interpol=this.interpol-1;if(this.pos==this.connection.pointsAmount()-3)
this.fieldB.fadeIn();if(this.pos==this.connection.pointsAmount()){clearInterval(this.intervalID);this.killArrowFade();}}}
this.killFade=function(){var fadeSpeed=900;var ref=this;this.element.fadeOut(fadeSpeed);if(this.connection!=undefined)
this.connection.element.fadeOut(fadeSpeed,function(){ref.kill()});}
this.kill=function(){this.fieldA.deactivate();this.fieldB.deactivate();this.connection.kill();}
this.killArrowFade=function(){var ref=this;this.element.fadeOut(300,function(){ref.killArrow()});}
this.killArrow=function(){this.element.html("");this.element.css("visibility","hidden");}
this.draw=function(){this.element.css("left",this.x-this.widthHalf);this.element.css("top",this.y-this.heightHalf);}
this.updateBG=function(){if($.browser.msie==true){return;}
if(this.oldDir==this.dir)
return;if(this.dir==UP)
this.element.css("background","url("+httpRoot+"images/traveller/animArrowT.gif)");if(this.dir==DOWN)
this.element.css("background","url("+httpRoot+"images/traveller/animArrowB.gif)");if(this.dir==RIGHT)
this.element.css("background","url("+httpRoot+"images/traveller/animArrowR.gif)");if(this.dir==LEFT)
this.element.css("background","url("+httpRoot+"images/traveller/animArrowL.gif)");}}
Traveller.prototype=new Rect(0,0,0,0);function HoverInfo(){this.element;this.inRect;this.inTopRect;this.inRectArrow;this.image;this.rectHeight;var dispLeft;var dispTop;this.init=function(parent){parent.append("<div id='hoverInfo' class='hoverInfo'></div>");this.element=$("#hoverInfo");this.element.append("<div id='inRectArrow' class='inRectArrow'></div>");this.inRectArrow=$("#inRectArrow");this.element.append("<div id='inTopRect' class='inTopRect'></div>");this.inTopRect=$("#inTopRect");this.element.append("<div id='inRect' class='inRect'></div>");this.inRect=$("#inRect");this.element.append("<div id='inBottomRectArrow' class='inBottomRectArrow'></div>");this.inBottomRectArrow=$("#inBottomRectArrow");this.element.append("<div id='inBottomRect' class='inBottomRect'></div>");this.inBottomRect=$("#inBottomRect");if($.browser.msie==true){this.inBottomRectArrow.hide();this.inRectArrow.show();this.inRectArrow.css("margin-left",30);this.inBottomRectArrow.css("margin-left",30);}
this.element.width(this.width);}
this.setData=function(title,links){if(title=="help"){this.inRect.html("<div style='height:220px;margin-left:-4px'><h3>Swisscom Labs Communitree</h3><img src='"+httpRoot+"images/scomImgs/legende.png' /></div>");return;}
var infoString="<h1>"+title+"</h1><ul>";for(var i=0;i<links.length;i++){infoString+="<li>"+links[i]+"</li>";}
infoString+="</ul>";this.inRect.html(infoString);}
this.setImage=function(src){if(src==undefined)
return;var html=this.inRect.html();html="<div id='imgContainer'><img src='"+src+"'/></div>"+html;this.inRect.html(html);}
this.moveTo=function(x,y,h){this.x=x;this.y=y;this.rectHeight=h;if($.browser.msie==true){this.draw();return;}
dispLeft=x>$(window).width()-this.width;dispTop=y>($(window).height()-master.element.offset().top)-this.height;if(dispLeft){this.inRectArrow.css("margin-left",130);this.inBottomRectArrow.css("margin-left",130);this.x=x-100;}else{this.inRectArrow.css("margin-left",30);this.inBottomRectArrow.css("margin-left",30);}
if(dispTop){this.inRectArrow.hide();this.inBottomRectArrow.show();this.y=y-h-this.element.height();}else{this.inBottomRectArrow.hide();this.inRectArrow.show();}
this.draw();}
this.show=function(){this.element.show();}
this.hide=function(){this.element.hide();}
this.draw=function(){this.element.css("left",this.x);this.element.css("top",this.y);}}
HoverInfo.prototype=new Rect(0,0,180,100);function Users(){this.cssClass="users";this.name="Users";this.idToUser=new Object();this.activeUser=undefined;this.load=function(){this.children=[];var method="communiuser?limit="+maxFields;var url=apiUrl+method;var usrs=this;log('load='+url);$.get(url,function(data){usrs.min=data["info"]["minPoints"];usrs.max=data["info"]["maxPoints"];usrs.diff=usrs.max-usrs.min;usrs.mult=110/usrs.max;usrs.min=usrs.min*usrs.mult;usrs.max=usrs.max*usrs.mult;usrs.diff=usrs.diff*usrs.mult;for(var i in data["users"]){var u=data["users"][i];usrs.addUser(u);}
usrs.processChildren();usrs.draw();if(usrs.activeUser!=0){var f=usrs.getVisibleByID(usrs.activeUser);if(f==undefined){f=usrs.getByID(usrs.activeUser);if(f!=undefined)
usrs.forceFitField(f);else{usrs.loadUser(usrs.activeUser);}}
if(f!=undefined)
f.setAsTheUser();}},'json');}
this.addUser=function(u){var f=new User();f.points=u.points;f.name=u.username;if(u.picture!="")
f.image=u.picture;f.joinDate=new Date(u.joined*1000);f.id=u.id;var s=easeOut((f.points-this.min)*this.mult,this.min,this.max,this.diff);if(s<14)
s=14;f.width=toGridX(s);f.height=toGridY(s);f.init(this);addNode(f.id,f);this.addChild(f);this.idToUser[f.id]=f;return f;}
this.loadUser=function(id){log('loadUser id='+id);var method="communiuser/".id;var url=apiUrl+method;var usrs=this;$.get(url,function(data){var f=usrs.addUser(data);usrs.forceFitField(f);if(data["id"]==usrs.activeUser)
f.setAsTheUser();},'json');}
this.setActiveUser=function(userID){this.activeUser=userID;}
this.fillDummyData=function(){this.children=[];var names=["Niklaus","Martin","Philip","undef","Konrad","Raphael","Sean","Tobi","Yummy Industries","This could be you"];for(var i=0;i<250;i++){var f=new User();f.points=randomi(1,300);f.name=names[randomi(0,names.length-1)];f.id=nextFieldID();var s=f.points/300*60;if(s<14)
s=14;f.width=toGridX(s);f.height=toGridY(s);f.init(this);this.addChild(f);}
this.processChildren();this.childrenFit[10].setAsTheUser();this.childrenFit[10].name="This is you";}}
Users.prototype=new Area();function User(){this.points=0;this.cssClass="user";this.joinDate;this.image;this.area="u";this.id=0;this.blocked=false;this.reference=this;this.getInfoData=function(){return["Points: "+this.points,"Registered: "+(this.joinDate.getDate())+"."+(this.joinDate.getMonth()+1)+"."+this.joinDate.getFullYear()];}
this.getImageData=function(){return this.image;}
this.onInit=function(){var ref=this;this.element.dblclick(function(){ref.onDblClick()});}
this.onClick=function(){var target=httpServer+"user/"+this.id;log('user.onDblClick target='+target);if($.browser.msie==true){$(document)[0].location=target;return;}
$(document)[0].location=target;}
this.drawActivity=function(){killTravellers();var limit=13;if($.browser.msie==true){limit=3;}
var args={uid:this.id,limit:limit};var ref=this;var method="communiactivity/"+this.id+"?limit="+limit;var url=apiUrl+method;$.get(url,function(data){ref.blocked=false;for(var i in data){var doTrav=true;var n=data[i];var f=undefined;var area=undefined;if(n.entitytype=="node"){area=master.nodes;}
else if(n.entitytype=="user")
area=master.users;if(area!=undefined){f=area.getVisibleByID(n.entityid);if(f==undefined){f=area.getByID(n.entityid);if(f==undefined){area.forceLoad(n.entityid,ref);}else{doTrav=area.forceFitField(f);}}}
if(doTrav){ref.startTraveller(f);}}},'json');}
this.startTraveller=function(f){var trav=new Traveller();if(f!=undefined)
trav.setFields(this,f);}
this.setAsTheUser=function(){this.element.addClass("theUser");this.defaultBG=httpRoot+"images/php/roundCorner.php?scale="+this.width+"&style=red&area="+this.area;this.hoverBG=this.defaultBG;this.activeBG=this.defaultBG;if($.browser.msie==true)
this.element.css('filter',"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+this.defaultBG+"', sizingMethod='scale')");else
this.element.css("background-image","url("+this.defaultBG+")");}}
User.prototype=new Field();function Nodes(){this.cssClass="nodes";this.name="Nodes";this.load=function(){this.children=[];var ntype="app"
var url=apiUrl+"communinode?limit="+maxFields;log('get='+url);var nws=this;$.get(url,function(data){nws.min=data["info"]["minPoints"];nws.max=data["info"]["maxPoints"];nws.diff=nws.max-nws.min;nws.mult=70/nws.max;nws.min=nws.min*nws.mult;nws.max=nws.max*nws.mult;nws.diff=nws.diff*nws.mult;for(var i in data["nodes"]){var u=data["nodes"][i];if(u){nws.ids.push(u.id);nws.addNode(u);}}
nws.draw();nws.processChildren();},'json');}
this.addNode=function(u){var f=new Node();var s=defNodesize;f.comments=u.comments;f.author=u.author;f.name=u.title;f.type=u.type;f.date=new Date(u.release_date*1000);f.id=u.id;f.rating=u.rating;if(f.rating>0){s=easeOut((f.rating-this.min)*this.mult,this.min,this.max,this.diff);}
if(s<minNodesize)
s=minNodesize;f.width=toGridX(s);f.height=toGridY(s);f.init(this);addNode(f.id,f);this.addChild(f);return f;}
this.forceLoad=function(id,refUsr){if(this.hasID(id))
return;this.ids.push(id);var url=apiUrl+"communinode/"+id;var nws=this;$.get(url,function(data){var f=nws.addNode(data);if(nws.forceFitField(f)){if(refUsr!=undefined){refUsr.startTraveller(f);}}else{}},'json');}
this.fillDummyData=function(){this.children=[];for(var i=0;i<30;i++){var f=new Node();f.points=randomi(1,300);f.name="some node";f.id=nextFieldID();var s=randomi(30,70);f.width=toGridX(s);f.height=toGridY(s);f.init(this);this.addChild(f);}
this.processChildren();}}
Nodes.prototype=new Area();function Node(){this.author="swisscom";this.cssClass="node";this.area="a";this.rating=0;this.comments=0;this.blocked=false;this.getInfoData=function(){var results=new Array();var i=0;results[i]="Category: "+this.type;i++;results[i]="Date: "+(this.date.getDay()+1)+"."+(this.date.getMonth()+1)+"."+this.date.getFullYear();i++;results[i]="by: "+this.author;i++;results[i]="<a href=/node/"+this.id+">Link to content</a>";i++;if(this.rating>0){results[i]="Hotness: "+this.rating;i++;}
if(this.comments>0){results[i]="Comments: "+this.comments;i++;}
return results;}
this.onClick=function(){log('node.onClick target='+target);var target=httpServer+"node/"+this.id;if($.browser.msie==true){$(document)[0].location=httpServer+target;return;}
$(document)[0].location=target;}}
Node.prototype=new Field();function SpacerField(){this.points=0;this.cssClass="spacerField";this.travellerPassThru=true;this.init=function(parent){}}
SpacerField.prototype=new Field();function Help(){this.area="help";this.id=-10;this.width=toGridX(42);this.height=toGridY(42);this.name="help";this.onClick=function(){};}
Help.prototype=new Field();function LabsMaster(){this.element;this.width=fullAreaWidth;this.height=fullAreaHeight;this.users=new Users();this.nodes=new Nodes();this.isVisible=false;this.clickElement;this.scb;this.init=function(){$("body").prepend("<div id='ctClickArea'>&nbsp;</div>");this.clickElement=$('#ctClickArea');this.clickElement.width($(window).width());this.clickElement.height($("#main").height());var ref=this;rootElement.append("<div id='ctLines'></div>");lineElement=$('#ctLines');rootElement.append("<div id='cTree'></div>");this.element=$('#cTree');this.initUsers();this.initNodes();var help=new Help();help.y=toGridY(this.height-help.height-10);help.x=toGridX(90);this.addChild(help);this.addChildFit(help);help.init(this);help.draw();if(enableToggleButton==true){$("body").append("<div id='showContentBtn'></div>");this.scb=$('#showContentBtn');this.scb.click(function(){ref.onClick()});this.scb.hide();}
if($.browser.msie==true){this.element.append("<div id='cTreeIE'>It appears you are running internet explorer. This browser may cause the communitree to run slower. <br />We recommend to use <a href='http://www.mozilla.com/firefox' target='_blank'>Firefox</a> or <a href='www.google.com/chrome' target='_blank'>Chrome</a> to have the full experience</div>");$("#cTreeIE").css("top",(this.height-15)+"px");$("#cTreeIE").css("width",this.width+"px");}
this.draw();this.isVisible=false;this.clickElement.click(function(){ref.onClick()});this.element.click(function(){ref.onClick()});this.clickElement=new clickMe();this.clickElement.draw();var refM=this;$("body").mousemove(function(event){var theX=event.pageX;if((theX+140)<$(window).width())
refM.clickElement.setPos(theX,event.pageY);else
refM.clickElement.setPosY(event.pageY+15);});$("#cTree").mouseover(function(){if(master.isVisible){$("#ctClick").hide();}else
$("#ctClick").show();});$("#ctClickArea").mouseover(function(){$("#ctClick").show();});$("#page").mouseover(function(){$("#ctClick").hide();});log("master.init done- "+this.width+"x"+this.height);}
this.draw=function(){lineElement.css("left","50%");lineElement.css("marginLeft",-this.width*.5);lineElement.width(this.width);lineElement.height(this.height);this.element.css("left","50%");this.element.css("marginLeft",-this.width*.5);this.element.width(this.width);this.element.height(this.height);}
this.hideContent=function(){showCommunitree();}
this.toggleContent=function(){if(contentIsShown=="no")
this.showContent();else
this.hideContent();}
this.showContent=function(){var ref=this;}
if(enableToggleButton==true){this.onClick=function(){this.toggleContent();}}
this.initUsers=function(){this.users.x=toGridX(0);this.users.y=toGridY(0);this.users.width=toGridX(this.width);this.users.height=toGridY(this.height);this.addChild(this.users);this.addChildFit(this.users);this.users.init(this);}
this.initNodes=function(){this.nodes.x=toGridX(nodeAreaX);this.nodes.y=toGridY(nodeAreaY);this.nodes.width=toGridX(nodeAreaWidth);this.nodes.height=toGridY(nodeAreaHeight);log('initNodes box:'+this.nodes.width+'w '+this.nodes.height+'h, '
+'pos:'+this.nodes.x+','+this.nodes.y);this.addChild(this.nodes);this.addChildFit(this.nodes);this.nodes.init(this);this.users.addDeadZone(this.nodes.x,this.nodes.y,this.nodes.width,this.nodes.height);}
this.getUsers=function(){log('getUsers ');this.users.load();}
this.getNode=function(id){log('getNode - empty function');}
this.loadData=function(){log('loadData users.load, nodes.load');this.users.load();this.nodes.load();if(typeof(communitree_activeUser)=='undefined'){communitree_activeUser=0;log('Use default communitree_activeUser='.communitree_activeUser);}
this.users.setActiveUser(communitree_activeUser);}
this.fillDummyData=function(){this.initUsers();this.initNodes();this.nodes.fillDummyData();this.nodes.draw();this.nodes.hideChildren(.4);this.users.fillDummyData();this.users.draw();}
this.randomConnection=function(){var a=this.randomChild().randomChild();var b=this.randomChild().randomChild();var trav=new Traveller();trav.setFields(a,b);}}
LabsMaster.prototype=new Distributor();var rootElement;var canvasElement;var lineElement;var UP=0;var RIGHT=1;var DOWN=2;var LEFT=3;function getDirection(a,b){if(a.x<b.x)
return RIGHT;if(a.x>b.x)
return LEFT;if(a.y>b.y)
return UP;if(a.y<b.y)
return DOWN;return RIGHT;}
var master=new LabsMaster();var info=new HoverInfo();var logger=new Logger();var idToFields=new Object();function addNode(id,field){idToFields[id]=field;}
var hash=$(document)[0].location.hash;if(hash!=""&&hash.search(/node/)!=-1){hash=hash.replace("#","");window.location=httpServer+hash;}
$(document).ready(function(){if(enableLogging){logger.init();logger.toggle();}
rootElement=$('#cTreeRoot');master.init();master.loadData();info.init($('#cTree'));info.hide();});function startDebugMode(){}
function log(m){window.console&&console.debug(m);if(!enableLogging)
return;logger.log(m);}