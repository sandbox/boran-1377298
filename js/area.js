/*
 * an area is a base element that can contain and maintain any number of fields, this class can be subclassed for better control
 */

function Area(){
	this.name = "NO NAME";
	this.element;
	this.areaNameDiv;
	this.cssClass = "emptyArea";
	this.id = 0;
	this.children = [];
	this.childrenFit = [];
	this.deadZones = [];
	this.isArea = true;
	this.minSize=0;
	this.maxSize=1;
	this.diffSize=1;
	this.mult = 1;
	this.ids = new Array();
	
	//init this area, parent must be of type labsMaster
	this.init = function(parent){
		this.id = nextAreaID();
		parent.element.append("<div id='area"+this.id+"' class='area "+this.cssClass+"'></div>");
		this.element = $("#area"+this.id);
    		//log('new area id=' +this.name +', x,y=' +this.x +',' +this.y +', width=' +this.width +', height=' +this.height);
		
		this.element.append("<div id='areaNameDiv"+this.id+"' class='areaNameDiv'><div id='blueArrow'></div><h1>"+this.name+"</h1><h2>"+this.width+"</h2></div>");
		this.areaNameDiv = $("#areaNameDiv"+this.id);
	}
	
	this.draw = function(){
		this.element = $("#area"+this.id);
		this.element.width(this.width);
		this.element.height(this.height);
		this.element.css("left",this.x);
		this.element.css("top",this.y);
	} 
	
	this.hasID = function(id){
		for(var i=0;i<this.ids.length;i++){
			if(this.ids[i] == id)
				return true;
		}
		return false;
	}
	
	//fill with some random data
	this.fillDummyData = function(){
		for(var i=0;i<20;i++){
			var f = new Field();
			f.id = nextFieldID();
			var s = randomi(30,80);
			f.width = toGridX(s);
			f.height = toGridY(s);
			f.init(this);
			this.addChild(f);
		}
		this.processChildren();
	}
	
	this.growAllChildren = function(){
		for(var i=0;i<this.childrenFit.length;i++){
			this.childrenFit[i].grow();
		}
	}
	
	this.fadeInAllChildren = function(){
		for(var i=0;i<this.childrenFit.length;i++){
			this.childrenFit[i].fadeIn();
		}
	}
	
	this.hideChildren = function(howMany){
		var amt = Math.round(this.childrenFit.length*howMany);
		for(var i=0;i<amt;i++){
			this.childrenFit[i].hide();
		}
	}
	
	this.hideAllChildren = function(){
		for(var i=0;i<this.childrenFit.length;i++){
			this.childrenFit[i].hide();
		}
	}
	
	this.showAllChildren = function(){
		for(var i=0;i<this.childrenFit.length;i++){
			this.childrenFit[i].show();
		}
	}
}

Area.prototype = new Distributor();
