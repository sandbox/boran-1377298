// Communitree configuration

// To test the communiverse much be active (but not necessarily communitree)

// Jquery: Handle '$ is undefined'
// http://www.adrogen.com/blog/jquery-conflict-is-not-a-function/
// needed when running with index.htm

// CONFIGURE:  this with your server name:
var domainName = "sislabsx.vptt.ch";    // DEV

var httpServer = "http://" +domainName +"/";   // do not change
var apiUrl   = httpServer+"api/";   // communiverse url

// where is this module?
var httpRoot = httpServer+"sites/all/modules/sandbox/communitree/";


var fullAreaWidth  = 900;   // complete canvas size
var fullAreaHeight = 600;  
// box for nodes, Users will take up remaining space
var minNodesize = 14; // no. pixes mini for smallest rated nodes
var defNodesize = 28; // standard node size if no rating
var nodeAreaWidth = 600;   // Specify the box used to display nodes
var nodeAreaHeight = 350;   //
var nodeAreaX = 10;   
var nodeAreaY = 10;   

var maxFields = 80;   // maximum number of nodes to pull
if($.browser.msie == true){
   maxFields = 20;    // IE can handle less
}

var enableLogging = false;   // production
//var enableLogging = true;   // TODO
var enableFirebugLogging = true;   

var enableToggleButton = false;   // hover button/link to hide/show the tree

var $ = jQuery.noConflict();


