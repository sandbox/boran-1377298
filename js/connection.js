function Connection(){
    this.a;
    this.b;
    this.root;
    this.curPos;
    
    this.points = [];
    
    this.id = nextConnectionID();
    
    this.passThru = [];
    
    lineElement.append("<div id='connection"+this.id+"' class='connection'></div>");
    this.element = $("#connection"+this.id);
    
    this.init = function(a, b){
        this.a = a;
        this.b = b;
        this.curPos = this.a;
        
        if(a.x == b.x && a.y == b.y)
            return;
        
        this.process();
    }
    
    this.addPassThru = function(p){
        this.passThru.push(p);
    }
    
    this.isPassThru = function(p){
        for(var i=0;i<this.passThru.length;i++){
            if(this.passThru[i] == p)
                return true;
        }
        return false;
    }
    
    this.pointAt = function(i){
        return this.points[i];
    }
    
    this.pointsAmount = function(){
        return this.points.length;
    }
    
    this.addPointEnd = function(p){
        this.points.push(p);
    }
    
    this.addPointBeginning = function(p){
        this.points.splice(0,0,p);
    }
    
    this.kill = function(){
        this.element.html("");
    }
    
    this.process = function(){
        this.points = [];
        var i=0;
        var preferX = true;
        do{
            var oldPos = new Point(this.curPos.x, this.curPos.y);
            
            this.points.push(new Point(this.curPos.x, this.curPos.y));
            
            var dX = this.b.x-this.curPos.x;
            var xDir = 0;
            if(dX != 0)
                xDir = dX/Math.abs(dX);
            
            var dY = this.b.y-this.curPos.y;
            var yDir = 0;
            if(dY != 0)
                yDir = dY/Math.abs(dY);
            
            var moveX = true;
            
            if(this.curPos.x == this.b.x)
                moveX = false;
            
            if(moveX)
                this.curPos.x += xDir*gridSizeX;
            else
                this.curPos.y += yDir*gridSizeY;
            
                
            var ids = master.hasChildrenAt(this.curPos.x, this.curPos.y);
            
            if(ids.length>0){ //we hit something
                for(var ii = 0; ii<ids.length;ii++){
                    var distMaster = master.childAt(ids[ii]);
                    var id = distMaster.hasChildAt(this.curPos.x-distMaster.x, this.curPos.y-distMaster.y);
                    if(id!=-1){
                        var dist = distMaster.childAt(id);
                        //if(this.isPassThru(dist)==false){
                            this.curPos = oldPos;
                            if(moveX){
                                var j = 0;
                                if(yDir == 0){
                                    if(Math.abs(this.curPos.y-dist.y)<Math.abs(this.curPos.y-dist.bottom()))
                                        yDir = 1;
                                    else
                                        yDir = -1;
                                }
                                while(this.curPos.y-distMaster.y>dist.y && this.curPos.y-distMaster.y<dist.bottom()){
                                    this.curPos.y += yDir*gridSizeY;
                                    this.points.push(new Point(this.curPos.x, this.curPos.y));
                                    j++;
                                    if(j>200){
                                        log("had to break while x avoiding obstacle");
                                        break;
                                    }
                                }
                                this.curPos.x += xDir*gridSizeX;
                            }else{
                                if(xDir == 0){
                                    if(Math.abs(this.curPos.x-dist.x)<Math.abs(this.curPos.x-dist.right()))
                                        xDir = 1;
                                    else
                                        xDir = -1;
                                }
                                var j = 0;
                                while(this.curPos.x-distMaster.x>dist.x && this.curPos.x-distMaster.x<dist.right()){
                                    this.curPos.x += xDir*gridSizeX;
                                    this.points.push(new Point(this.curPos.x, this.curPos.y));
                                    j++;
                                    if(j>200){
                                        log("had to break while y avoiding obstacle");
                                        break;
                                    }
                                }
                                this.curPos.y += yDir*gridSizeY;
                            }
                        //}
                    }
                }
            }
            i++;
            if(i>200){
                log("had to break pathfinder while moving to target, i got over a thousand! xDir:"+xDir+" moveX:"+moveX+" yDir:"+yDir);
                break;
            }
        }while(this.curPos.x != this.b.x || this.curPos.y != this.b.y);
        this.points.push(this.b);
    }
    
    this.draw = function(){
        var cX = this.points[0].x;
        var cY = this.points[0].y;
        
        
        //needs optimization to save some divs
        for(var i=1;i<this.pointsAmount();i++){
            this.drawStep(i);
        }
    }
    
    this.drawStep = function(step){
        var a = this.pointAt(step-1);
        var b = this.pointAt(step);
        var w = Math.abs(a.x-b.x);
        var h = Math.abs(a.y-b.y);
        var x = a.x;
        if(b.x<a.x)
            x = b.x;
        
        var y = a.y;
        if(b.y<a.y)
            y = b.y;
        
        var cl = "";
        

        if(w == 0)
            w = 1;
        if(h == 0)
            h = 1;
            
        if(w < h)
            cl = "h";
        else
            cl = "w";
    
        
        
        this.element.append("<div class='"+cl+"' style='width:"+w+"px;height:"+h+"px;top:"+y+"px;left:"+x+"px'></div>");
    }
}