/**
 * doc?
 */
function Distributable(){
	this.parent = null;
	this.draw = function(){};
	this.hide = function(){};
	this.absPos = function(){
		var p = new Point(this.x, this.y);
		var cur = this;
		while(cur.parent != null){
			p.x += cur.parent.x;
			p.y += cur.parent.y;
			cur = cur.parent;
		}
		return p;
	}
}
Distributable.prototype = new Rect(0,0,0,0);

//This class arranges a set of rectangles so they can't overlap
function Distributor(){
	this.children = [];
	this.childrenFit = [];
	this.deadZones = [];
	this.lastFit = 0;
	
	this.alert = function(){
		alert("OK");
	}
	
	//Call this to add a child to the distribution list, r must be of type Distributable
	this.addChild = function(r){
		r.parent = this;
		this.children.push(r);
	}
	
	this.addChildFit = function(r){
		this.childrenFit.push(r);
	}
	
	this.childAt = function(id){
		return this.childrenFit[id];
	}
	
	//function to determine if a certain point is within a child, returns id of child or -1 if free
	this.hasChildAt = function(x, y){
		for(var i=0;i<this.childrenFit.length;i++){
			if(this.childrenFit[i].contains(x,y))
				return i;
		}
		return -1;
	}
	
	this.hasChildrenAt = function(x, y){
		var ret = [];
		for(var i=0;i<this.childrenFit.length;i++){
			if(this.childrenFit[i].contains(x,y))
				ret.push(i);
		}
		return ret;
	}
	
  // doc?
	this.addDeadZone = function(x, y, w, h){
		var d = new Rect(x, y, w, h);
		this.deadZones.push(d);
	}
	
  // doc?
	this.intersectsDeadZone = function(r){
		for(var j=0;j<this.deadZones.length;j++){
				if(r.intersects(this.deadZones[j])){
					return true;
				}
		}
		return false;
	}
	
	// After adding all children call this function to process the distribution
  // which calls arrangeChildren > fitChild
	this.processChildren = function(){
		this.arrangeChildren();
	}
	
	//after processing fit each child into the available area
	this.arrangeChildren = function(){
		var dropped = 0;
		for(var i=0;i<this.children.length;i++){
			var d = this.children[i];
			if(!this.fitChild(d)) {
				dropped++;   // child would not fit in
        //log('distributor.arrangeChildren ' +this.name +' could not fit: ' +print_r(d));
      }
		}
		log('distributor.arrangeChildren ' +this.name +' ' 
      +this.children.length +" Dropped "+dropped);
	}
    
    getSpiralPos = function(cx, cy, radius, windings, imax, i){
		var offset = radius/(windings*360);
		var angle = map(i,0,imax,0, 360 * windings);
		
		var currentRadius = angle * offset;
		var x = cx + currentRadius * Math.cos(angle);
		var y = cy + currentRadius * Math.sin(angle);

		return new Point(x,y);	
	}
	
  // Fit the child d into the available space?
	this.fitChild = function(d){
		var fits = false;
		var count = 0;
		var maxCount = 80;   // try 80 different positions?
		if($.browser.msie == true){
			maxCount = 20;
		}
		while(fits == false && count<maxCount){
			
			//d.x = toGridX(randomi(gridSizeX,this.width-d.width));
			//d.y = toGridY(randomi(gridSizeY,this.height-d.height));
			var mW;
			if(this.width>this.height) mW = this.height;
			else mW = this.width;
			
			//var spiralPos = getSpiralPos(this.width/2, this.height/2, mW/2, maxCount/5, maxCount, count);
			var spiralPos = getSpiralPos(this.width/2, this.height/2, mW/2, randomi(3,33), maxCount, count);
			d.x = toGridX(spiralPos.x);
			d.y = toGridY(spiralPos.y);
            
			if(d.x>this.width-d.width || d.y>this.height-d.height){
				fits = false;
			}else{
				var sects = false;
				if(this.intersectsDeadZone(d) == false){
					for(var j=0;j<this.childrenFit.length;j++){
						if(this.childrenFit[j]!=d)
							if(d.intersectsWithSpace(this.childrenFit[j]))
								sects = true;
					}
				}else{
					sects = true;
				}
				
				fits = !sects;
			}
			count++;
		}
		
		if(!fits){
			//d.width = d.height = d.x = d.y = 0;
			d.hide();
			return false;
		}else{
			d.draw();
			this.childrenFit.push(d);
			return true;
		}
		return false;
	}

	
	this.randomChild = function(){
		return this.childrenFit[randomi(0,this.childrenFit.length-1)];
	}
	
	this.forceFitField = function(f){
		var fits = false;
		var count = 0;
		var maxCount = 70;
		while(fits == false && count<maxCount){
			var sects = false;
			f.x = toGridX(randomi(0,this.width-f.width));
			f.y = toGridY(randomi(0,this.height-f.height));
			if(this.intersectsDeadZone(f) == false){
				for(var j=0;j<this.childrenFit.length;j++){
					if(this.childrenFit[j]!=f)
						if(f.intersectsWithSpace(this.childrenFit[j]))
							sects = true;
				}
			}else{
					sects = true;
			}	
			fits = !sects;
			count++;
		}
		
		if(!fits){
			for(var j=0;j<this.childrenFit.length;j++){
				var c = this.childrenFit[j];
				if(c.width>=f.width){
					c.element.fadeOut();
					this.childrenFit.splice(j, 1);
					this.childrenFit.push(f);
					f.x = c.x;
					f.y = c.y;
					fits = true;
					break;
				}
			}
		}
		if(fits){
			f.draw();
			f.hide();
			f.fadeIn();
			this.childrenFit.push(f);
		}
		return fits;
	}
	
	this.getVisibleByID = function(id){
		for(var j=0;j<this.childrenFit.length;j++){
			if(this.childrenFit[j].id == id)
				return this.childrenFit[j];
		}
		return undefined;
	}
	
	this.getByID = function(id){
		for(var j=0;j<this.children.length;j++){
			if(this.children[j].id == id)
				return this.children[j];
		}
		return undefined;
	}
}

//The distributor inherits Distributable itsself
Distributor.prototype = new Distributable(0,0,0,0);
