/*
 * A field is the base box within an area in the communitree
 * Users, Nodes and the help boex are fields, so they inherit and extend Field.
 * TODO: extend it to have other designs, functionality etc...
 */

function Field(){
	this.name = "No Name";
	this.element;
	this.inRect;
	this.cssClass = "fieldBlank";
	this.isArea = false;
	this.area = "";
	
	var offset = 2;
	
	this.visible = true;
	this.isActive = false;
	
	//init this area, parent must be of type labsMaster
	this.init = function(parent){
                //log('Field init: area=' +this.area +', id=' +this.id +'parent=' +parent.name);
		parent.element.append("<div id='field"+this.area+this.id+"' class='field "+this.cssClass+"' ></div>");
		this.element = $("#field"+this.area+this.id);
		
		//this.element.append("<div id='inRect"+this.id+"' class='inRect'></div>");
		//this.inRect = $("#inRect"+this.id);
		
		var ref = this;
		this.element.hover(function(){ref.onMouseOver()},function(){ref.onMouseOut()});
		this.element.click(function(){ref.onClick()});
		
		this.defaultBG = httpRoot+"images/php/roundCorner.php?scale="+this.width+"&area="+this.area;
		this.hoverBG = httpRoot+"images/php/roundCorner.php?scale="+this.width+"&style=hover&area="+this.area;
		this.activeBG = httpRoot+"images/php/roundCorner.php?scale="+this.width+"&style=hover&area="+this.area;
		this.onInit();
	}
	
	this.onInit = function(){
		
	}
	
	this.onMouseOver = function(){
		//this.inRect.addClass('inRectOver');
		this.element.addClass('fieldOver');
		if($.browser.msie == true)
			this.element.css('filter', "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+this.hoverBG+"', sizingMethod='scale')");
		else
			this.element.css("background-image", "url("+this.hoverBG+")");
		info.setData(this.name, this.getInfoData());
		info.setImage(this.getImageData());
		info.moveTo(this.x+this.parent.x+this.width/2,this.y+this.parent.y+this.height, this.height);
		info.show();

    this.drawActivity();   // TODO
	}
	
	this.getImageData = function(){
		return undefined;
	}
	
	this.getInfoData = function(){
		return ["Id: "+this.id, "Width: "+this.width, "Pos: "+ this.x +"x /" +this.y+"y"];
	}
	
	this.onMouseOut = function(){
		info.hide();
		//this.inRect.removeClass('inRectOver');
		this.element.removeClass('fieldOver');
		if(this.isActive == false){
			if($.browser.msie == true)
				this.element.css('filter', "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+this.defaultBG+"', sizingMethod='scale')");
			else
				this.element.css("background-image", "url("+this.defaultBG+")");
		}
	}
	
	this.onClick = function(){
		
  //toogelTree();//from toggle.js by LS
	
	//ORIGINAL CODE	
/*
		var target = "node/"+this.id;
                log('field.onClick target=' +target);
		if($.browser.msie == true){
			$(document)[0].location = httpServer+target;
			return;
		}
		master.showContent();
		$(document)[0].location.hash = target;
		$('#content-inner').html("<div style='width: 100%; text-align:left;margin-top: 50px'><img src='"+httpServer+"sites/all/modules/ctools/images/status-active.gif' /> loading...</div>");
		$('#content-inner').load(httpServer+target+"?noHeadFoot");
*/
		//window.open(httpServer+"node/"+this.id);
	}
	
	this.grow = function(){
		this.element.width(0);
		this.element.height(0);
		this.element.css("left", this.x+offset);
		this.element.css("top", this.y+offset);
		this.element.animate({
			width: this.width-1-offset*2,
			height: this.height-1-offset*2,
			left: this.x+offset,
			top: this.y+offset
		},800, function(){});
	}
	
	this.fadeIn = function(){
		var ref = this;
		this.element.fadeIn(1400, function(){ref.show()});
	}
	
	this.fadeOut = function(){
		var ref = this;
		this.element.fadeOut(1100, function(){ref.hide()});
	}
	
	this.hide = function(){

		//this.inRect.addClass('inRectActive');
		//this.element.addClass('fieldActive');
		this.visible = false;
		this.element.hide();
	}
	
	this.show = function(){
		//this.inRect.removeClass('inRectActive');
		//this.element.removeClass('fieldActive');
		this.visible = true;
		this.element.show();
	}
	
	this.activate = function(){
		//this.inRect.addClass('inRectActive');
		this.isActive = true;
		this.element.addClass('fieldActive');
		if($.browser.msie == true)
			this.element.css('filter', "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+this.activeBG+"', sizingMethod='scale')");
		else
			this.element.css("background-image", "url("+this.activeBG+")");
	}
	
	this.deactivate = function(){
		//this.inRect.removeClass('inRectActive');
		this.isActive = false;
		this.element.removeClass('fieldActive');
		if($.browser.msie == true)
			this.element.css('filter', "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+this.defaultBG+"', sizingMethod='scale')");
		else
			this.element.css("background-image", "url("+this.defaultBG+")");
	}
	
	this.draw = function(){
		this.element.width(this.width);
		this.element.height(this.height);
		this.element.css("left",this.x);
		this.element.css("top",this.y);
		
		//this.element.css("background-image", this.defaultBG);
		//this.element[0].runtimeStyle.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + this.defaultBG + "',sizingMethod='scale')";
		if($.browser.msie == true)
			this.element.css('filter', "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+this.defaultBG+"', sizingMethod='scale')");
		else
			this.element.css("background-image", "url("+this.defaultBG+")");
		//this.inRect.width(this.width-9-offset*2);
		//this.inRect.height(this.height-9-offset*2);
	}
	
	this.getCenter = function(){
		var ret = new Point();
		ret.x = this.centerXi()+this.parent.x;
		ret.y = this.centerYi()+this.parent.y;
		return ret;
	}
	
	this.getConnectionStart = function(p){
		var ret = new Point();
		var dX = p.x - this.x;
		var dY = p.y - this.y;
		if(Math.abs(dX)<Math.abs(dY)){
			if(dX<0)
				ret.x = this.x;
			else
				ret.x = this.right();
			ret.y = toGridY(this.y+this.height*.5);
		}else{
			if(dY>0)
				ret.y = this.y;
			else
				ret.y = this.bottom();
			ret.x = toGridX(this.x+this.width*.5);
		}
		ret.x += this.parent.x;
		ret.y += this.parent.y;
		return ret;
	}
	
	this.hide = function(){
		this.element.hide();
	}
}

Field.prototype = new Distributable();
