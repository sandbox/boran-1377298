

function HoverInfo(){

    this.element;
    this.inRect;
    this.inTopRect;
    this.inRectArrow;
    this.image;
    
    this.rectHeight;
    var dispLeft;
    var dispTop;
    
    this.init = function(parent){
	    parent.append("<div id='hoverInfo' class='hoverInfo'></div>");
	    this.element = $("#hoverInfo");
            
            this.element.append("<div id='inRectArrow' class='inRectArrow'></div>");
	    this.inRectArrow = $("#inRectArrow");
            
            this.element.append("<div id='inTopRect' class='inTopRect'></div>");
	    this.inTopRect = $("#inTopRect");
            
            this.element.append("<div id='inRect' class='inRect'></div>");
	    this.inRect = $("#inRect");
	    
	    this.element.append("<div id='inBottomRectArrow' class='inBottomRectArrow'></div>");
	    this.inBottomRectArrow = $("#inBottomRectArrow");
            
            this.element.append("<div id='inBottomRect' class='inBottomRect'></div>");
	    this.inBottomRect = $("#inBottomRect");
	    
	    if($.browser.msie == true){	
		this.inBottomRectArrow.hide();
		this.inRectArrow.show();
		this.inRectArrow.css("margin-left",30);
		this.inBottomRectArrow.css("margin-left",30);
		}
		this.element.width(this.width);
    }
    

    /* What data is show in the hover boxes? 
     */
    this.setData = function(title, links){

	/* help box */
	if(title == "help"){
		this.inRect.html("<div style='height:220px;margin-left:-4px'><h3>Swisscom Labs Communitree</h3><img src='"+httpRoot+"images/scomImgs/legende.png' /></div>");
		return;
	}
	/* other boxes */
        var infoString = "<h1>"+title+"</h1><ul>";
        for(var i=0; i<links.length; i++){
            infoString += "<li>"+links[i]+"</li>";
        }
        infoString += "</ul>";
        this.inRect.html(infoString);
    }
    
    this.setImage = function(src){
	if(src==undefined)
		return;
	var html = this.inRect.html();
	html = "<div id='imgContainer'><img src='"+src+"'/></div>"+html;
	this.inRect.html(html);
    }
    
    this.moveTo = function(x,y,h){
        this.x = x;
        this.y = y;
	this.rectHeight = h;
	if($.browser.msie == true){
		this.draw();
		return;
	}
	dispLeft = x>$(window).width()-this.width;
	dispTop = y>($(window).height()-master.element.offset().top)-this.height;
	
	if(dispLeft){
		//this.inRectArrow.show();
		this.inRectArrow.css("margin-left",130);
		this.inBottomRectArrow.css("margin-left",130);
		this.x = x - 100;
	}else{
		this.inRectArrow.css("margin-left",30);
		this.inBottomRectArrow.css("margin-left",30);
	}
	
	if(dispTop){
		this.inRectArrow.hide();
		this.inBottomRectArrow.show();
		this.y = y -h -this.element.height();
	}else{
		this.inBottomRectArrow.hide();
		this.inRectArrow.show();
	}
	
	this.draw();
    }
    
    this.show = function(){
	this.element.show();
    }
    
    this.hide = function(){
        this.element.hide();
    }
    
    this.draw = function(){
        
	//this.element.height(this.height);
        this.element.css("left",this.x);
	this.element.css("top",this.y);	
    }
    
}
HoverInfo.prototype = new Rect(0,0,180,100);
