/*
 * labsMaster.js
 */

function Help(){
	this.area  = "help";
	this.id = -10;
	this.width = toGridX(42);
	this.height = toGridY(42);
	this.name = "help";
	
	this.onClick = function(){};
}
Help.prototype = new Field(); 


function LabsMaster(){
	this.element;
	//this.width = 1100;    // Drawing area. TODO: make configurable?
	//this.height = 700;    // TODO: make configurable?
	this.width = fullAreaWidth;    // Drawing area.
	this.height = fullAreaHeight;  
	this.users = new Users();
	this.nodes = new Nodes();
	this.isVisible = false;
	this.clickElement;
	this.scb;
	
	//initiate
	this.init = function(){
		$("body").prepend("<div id='ctClickArea'>&nbsp;</div>");
		this.clickElement = $('#ctClickArea');
		this.clickElement.width($(window).width());
		this.clickElement.height($("#main").height());
		
		var ref = this;
		
		rootElement.append("<div id='ctLines'></div>");
		lineElement = $('#ctLines');
		rootElement.append("<div id='cTree'></div>");
		this.element = $('#cTree');
		

		this.initUsers();
		this.initNodes();
		//this.initApps();
		//this.initNews();
		//this.initIdeas();
		//#this.initTrials();
		
		var help = new Help();
		help.y = toGridY(this.height-help.height-10);
		help.x = toGridX(90);
		this.addChild(help);
		this.addChildFit(help);
		help.init(this);
		help.draw();

		
    if (enableToggleButton == true) {
		  $("body").append("<div id='showContentBtn'></div>");
		  this.scb = $('#showContentBtn');
		  this.scb.click(function(){ref.onClick()});
		  this.scb.hide();
    }
		
		if($.browser.msie == true){
			this.element.append("<div id='cTreeIE'>It appears you are running internet explorer. This browser may cause the communitree to run slower. <br />We recommend to use <a href='http://www.mozilla.com/firefox' target='_blank'>Firefox</a> or <a href='www.google.com/chrome' target='_blank'>Chrome</a> to have the full experience</div>");
			$("#cTreeIE").css("top", (this.height-15)+"px");
			$("#cTreeIE").css("width", this.width+"px");
		}
		
		this.draw();
		this.isVisible = false;
		
		this.clickElement.click(function(){ref.onClick()});
		
		this.element.click(function(){ref.onClick()});
		
		this.clickElement = new clickMe();
		this.clickElement.draw();
		var refM = this;
		$("body").mousemove(function(event) {
			var theX = event.pageX;
			/*if(event.pageY<130)
				$("#ctClick").hide();
			else if(event.pageY>130 && event.pageY<150)
				$("#ctClick").show();*/
			if((theX+140)<$(window).width())
				refM.clickElement.setPos(theX, event.pageY);
			else
				refM.clickElement.setPosY(event.pageY+15);
		});
		
		$("#cTree").mouseover(function() {
			if(master.isVisible){
				$("#ctClick").hide();
			}else
				$("#ctClick").show();
		});
		
		$("#ctClickArea").mouseover(function() {
			$("#ctClick").show();
		});
		
		$("#page").mouseover(function() {
			$("#ctClick").hide();
		});
		
		log("master.init done- "+this.width+"x"+this.height);
	}
	
	//update positioning
	this.draw = function(){
		//log('labsMaster.draw');
		/* METHOD FOR DRAWING WITH jsDraw2D
		canvasElement.css("left", "50%");
		canvasElement.css("marginLeft", -this.width*.5);
		canvasElement.width(this.width);
		canvasElement.height(this.height);
		*/
		
		lineElement.css("left", "50%");
		lineElement.css("marginLeft", -this.width*.5);
		lineElement.width(this.width);
		lineElement.height(this.height);
		
		this.element.css("left", "50%");
		this.element.css("marginLeft", -this.width*.5);
		this.element.width(this.width);
		this.element.height(this.height);
	}
	
	this.hideContent = function(){

	//ORIGINAL CODE	
/*
		this.element.unbind("click");
		//$('#main').hide();$('#footer').hide();
		$('.region-inner-content').hide()
		this.isVisible = true;
		this.clickElement.setInner("show content");
		$(document)[0].location.hash = "main-content";
		this.scb.show();
		$("#ctClick").hide();
*/

  	  showCommunitree();
	}
	
	this.toggleContent = function(){
    // disable the tree hiding when one clicks on the background
    if(contentIsShown == "no")
			this.showContent();
		else
			this.hideContent();
	}
	
	this.showContent = function(){
		var ref = this;
		//TODO hideCommunitree();

	//ORIGINAL CODE	
		//$('#main').fadeIn();$('#footer').fadeIn('200', function(){ref.isVisible = false;});
/* 		$('.region-inner-content').hide(); */
		
/*
		this.clickElement.setInner("show communitree");
		var ref = this;
		this.element.click(function(){ref.onClick()});
		$(document)[0].location.hash = "";   // NEW
		this.scb.hide();
		$("#ctClick").show();
*/
	}
	
  if (enableToggleButton == true) {
	  this.onClick = function(){
	  	this.toggleContent();
	  }
  }
	
	this.initUsers = function(){
		//log('initUsers ');	
		this.users.x = toGridX(0);
		this.users.y = toGridY(0);
		this.users.width = toGridX(this.width);
		this.users.height = toGridY(this.height);
		this.addChild(this.users);
		this.addChildFit(this.users);
		this.users.init(this);
	}
	
  // Nodes: draw in a zone fro nodes, and explude users
  this.initNodes = function(){
        //this.nodes.x = toGridX(50);   
        //this.nodes.y = toGridY(50);
        //this.nodes.width = toGridX(600);
        //this.nodes.height = toGridY(400);
        this.nodes.x = toGridX(nodeAreaX);   
        this.nodes.y = toGridY(nodeAreaY);
        this.nodes.width = toGridX(nodeAreaWidth);
        this.nodes.height = toGridY(nodeAreaHeight);
		    log('initNodes box:' +this.nodes.width +'w ' +this.nodes.height +'h, '
            +'pos:' +this.nodes.x +',' + this.nodes.y);	

        this.addChild(this.nodes);
        this.addChildFit(this.nodes);
        this.nodes.init(this);

        // user objects should not be drawn in the node zone
        this.users.addDeadZone(this.nodes.x, this.nodes.y, this.nodes.width, this.nodes.height);
        //this.users.addChild(sf);
        //this.users.addChildFit(sf);
  }

	this.getUsers = function(){
		log('getUsers ');	
		this.users.load();
	}
	
	this.getNode = function(id){
		log('getNode - empty function');	
	}
	
	this.loadData = function(){
   	//this.fillDummyData();  // is this where test should be called?
		log('loadData users.load, nodes.load');	
		this.users.load();
		this.nodes.load(); 
		
    if (typeof(communitree_activeUser)=='undefined') {
      //communitree_activeUser = 1;  // default to UID=1 (admin)
      communitree_activeUser = 0;  // default to zero (anony) 
      log('Use default communitree_activeUser=' . communitree_activeUser);
    }
		this.users.setActiveUser(communitree_activeUser);  // read global from Drupal
	}
	
	//fill with some random data: how well does it work?
	this.fillDummyData = function(){
		this.initUsers();
		this.initNodes();
		
		this.nodes.fillDummyData();
		this.nodes.draw();
		this.nodes.hideChildren(.4);
		
		this.users.fillDummyData();
		this.users.draw();
		
	}
	
	this.randomConnection = function(){
		var a = this.randomChild().randomChild();
		var b = this.randomChild().randomChild();
		var trav = new Traveller();
		trav.setFields(a, b);
		//canvas.drawLine(penDefault, a.absPos(), b.absPos());
	}
}

LabsMaster.prototype = new Distributor();

