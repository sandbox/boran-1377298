// TODO: document
//
// Append log messages to a div called loggerContent
function Logger(){
	var el;
	var elRoot;
		
	this.init = function(){
		$('body').append("<div id='logger'><div id='loggerContent'></div></div>");
		elRoot = $('#logger');
		elRoot.css("zIndex",99);
		//elRoot.bind('click', this.toggle); // ignore
		//$('body').bind('keyup', keyUp);
		el = $('#logger #loggerContent');
		this.toggle();
	}
	
	this.log = function(m){
		el.append("> "+m+"<br />");
	}
	
	this.toggle = function(){
		elRoot.toggle();
	}
	
/*
	function keyUp(event){
		if(event.keyCode == 76)
			elRoot.toggle();
	}*/
}

/**
 * Function : dump()
 * Arguments: The data - array,hash(associative array),object
 *    The level - OPTIONAL
 * Returns  : The textual representation of the array.
 * This function was inspired by the print_r function of PHP.
 * This will accept some data as the argument and return a
 * text that will be a more readable version of the
 * array/hash/object that is given.
 * Docs: http://www.openjs.com/scripts/others/dump_function_php_print_r.php
 */
function dump(arr,level) {
	var dumped_text = "";
	if(!level) level = 0;
	
	//The padding given at the beginning of the line.
	var level_padding = "";
	for(var j=0;j<level+1;j++) level_padding += "    ";
	
	if(typeof(arr) == 'object') { //Array/Hashes/Objects 
		for(var item in arr) {
			var value = arr[item];
			
			if(typeof(value) == 'object') { //If it is an array,
				dumped_text += level_padding + "'" + item + "' ...\n";
				//dumped_text += dump(value,level+1);  // TODO: recurse
			} else {
				dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
			}
		}
	} else { //Stings/Chars/Numbers etc.
		dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
	}
	return dumped_text;
}

