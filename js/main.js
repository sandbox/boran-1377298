/**
 * main.js
 */

//the root element is the basic div, where everything will be appended to, defined on document ready
var rootElement;
//the canvas element is the base for all drawing of overlay elements
var canvasElement;

var lineElement;

//DIRECTIONS
var UP = 0;
var RIGHT = 1;
var DOWN = 2;
var LEFT = 3;

function getDirection(a, b){
    if(a.x<b.x)
        return RIGHT;
    if(a.x>b.x)
	return LEFT;
    if(a.y>b.y)
	return UP;
    if(a.y<b.y)
	return DOWN;
    return RIGHT;
}
/*
//the jsDraw2D instance
var canvas;
//the default pen for drawing
var penDefault = new jsPen(new jsColor("#11AAFF"), 1);
var penRed = new jsPen(new jsColor("#ff0000"), 1);
var penGreen = new jsPen(new jsColor("#00ff00"), 2);
var penBlue = new jsPen(new jsColor("#0000ff"), 2);
*/

//the master contains a number of areas and arranges them
var master = new LabsMaster();

//info hover box
var info = new HoverInfo();

//logging related
var logger = new Logger();

// see also config.js for settings

var idToFields = new Object();


function addNode(id, field){
    //if (field.cssClass!='user') {
    //  log('addNode id=' +id +', DUMP=' +dump(field));
    //}
    idToFields[id] = field;
}



var hash = $(document)[0].location.hash;
if(hash != "" && hash.search(/node/)!=-1){
    hash = hash.replace("#","");
    window.location = httpServer+hash;
}

//THE MAIN STARTING POINT
$(document).ready(function(){
    if(enableLogging){
    	logger.init();
	logger.toggle();
    }
    rootElement = $('#cTreeRoot');
    //log('main.js: master.init()');
    master.init();
    
    //log('master.loadData()');
    master.loadData();
    //master.getUsers();
    //master.processChildren();
    
    info.init($('#cTree'));
    info.hide();
    
    /*if(hash != "" && hash.search(/hideContent/)!=-1){
	master.hideContent();
    }*/
});

function startDebugMode(){
        
}

function log(m){
	window.console && console.debug(m);   // log to the firebug console, if enabled
//enableFirebugLogging
	if(!enableLogging)
		return;
	logger.log(m);
}
