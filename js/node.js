/**
 * node.js
 **/
function Node(){
    this.author = "swisscom";
    this.cssClass = "node";
    this.area = "a";
    this.rating = 0;
    this.comments = 0;
    this.blocked = false;
    
    this.getInfoData = function(){
        var results=new Array();
        var i=0; // array index
        //results[i]="Category: XX";     i++;
        results[i]="Category: " +this.type;     i++;
        results[i]="Date: "+(this.date.getDay()+1)+"."+(this.date.getMonth()+1)+"."+this.date.getFullYear(); i++;
        results[i]="by: "+this.author; i++;
        results[i]="<a href=/node/" +this.id +">Link to content</a>"; i++;
        if (this.rating>0) {
  	  results[i]="Hotness: "+this.rating; i++;
	}
        if (this.comments>0) {
  	  results[i]="Comments: "+this.comments; i++;
	}

	return results;
    }


    this.onClick = function(){
        log('node.onClick target=' +target);
        //toggleTree();             //stop tree from disappearing
        var target = httpServer +"node/" +this.id;
        if($.browser.msie == true){
                $(document)[0].location = httpServer+target;
                return;
        }
        //master.showContent();
        //$(document)[0].location.hash = target;    // TODO does not work with index.html?
        $(document)[0].location = target;
        //$('#content-inner').html("<div style='width: 100%; text-align:left;margin-top: 50px'><img src='"+httpServer+"sites/all/modules/ctools/images/status-active.gif' /> loading...</div>");
        //$('#content-inner').load(httpServer+target+"?noHeadFoot");
    }

}

Node.prototype = new Field();
