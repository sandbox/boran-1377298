function App(){
    this.developer = "swisscom";
    //this.launchDate = new Date(randomi(2006, 2010), randomi(1,12), randomi(1,30));
    this.cssClass = "app";
    this.area = "a";
    this.rating = 0;
    this.comments = 0;
    
    this.getInfoData = function(){
        var results=new Array();
        var i=0; // array index
        results[i]="Category: Apps";     i++;
        results[i]="Date: "+(this.date.getDay()+1)+"."+(this.date.getMonth()+1)+"."+this.date.getFullYear(); i++;
        results[i]="by: "+this.developer; i++;
        if (this.rating>0) {
  	  results[i]="Hotness: "+this.rating; i++;
	}
        if (this.comments>0) {
  	  results[i]="Comments: "+this.comments; i++;
	}

	//return ["Category: Apps","Developer: "+this.developer, "Launch: "+(this.launchDate.getDay()+1)+"."+(this.launchDate.getMonth()+1)+"."+this.launchDate.getFullYear()];
	/*return ["Category: Apps",
          "by: "+this.developer, 
          "Hotness: "+this.rating, 
          this.commenttext,
          "Date: "+(this.date.getDay()+1)+"."+(this.date.getMonth()+1)+"."+this.date.getFullYear()
        ]; */
	return results;
    }
}

App.prototype = new Field();
