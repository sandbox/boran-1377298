/*
 DISPLAY AND RETREIVE USERS
*/

function Apps(){
    this.cssClass = "apps";
    this.name = "Apps";
    
    this.load = function(){
        this.children = [];
        /*var args = {};
        var domain_name = domainName;
        var hash;
        var nonce = randomi(23,999999);
        var domain_time_stamp =  Math.round(new Date().getTime() / 1000);
        var sessid = "7af6687c1c89a47a3d59f98e2de07dff";
        var method = "communiverse.getappslist";
        var api_key = "0faa196d269711df46fc0eea2c301c58";
        var url = httpRoot+"utils/hashHmac.php?dts="+domain_time_stamp+"&dn="+domain_name+"&nonce="+nonce+"&method="+method+"&api_key="+api_key;*/
        var ntype  = "app"
        var method = "communinode?limit=" +maxFields + "&ntype=" +ntype;    // get nodes
        var url = apiUrl +method;  // TODO: authentication

        var nws = this;
        //log('apps.load url=' + url);
        $.get(url, function(data){
                    nws.min = data["info"]["minPoints"];
                    nws.max = data["info"]["maxPoints"];
                    nws.diff = nws.max-nws.min;
                    nws.mult = 70/nws.max;
                    nws.min = nws.min*nws.mult;
                    nws.max = nws.max*nws.mult;
                    nws.diff = nws.diff*nws.mult;
                    for(var i in data["nodes"]){
                        var u = data["nodes"][i];
                        nws.ids.push(u.id);
                        //log('apps.load add: ' +u.title);
                        nws.addApp(u);
                    }
                    nws.draw();
                    nws.processChildren();
        }, 'json');
    }
    
    this.addApp = function(u){
        var f = new App();
        var s = defNodesize;     // default size of Node
        f.comments = u.comments;
        f.developer = u.author;
        f.name = u.title;
        f.date = new Date(u.release_date*1000);
        f.id = u.id;
        f.rating = u.rating;
        if (f.rating > 0) {  // if there is a rating, dynamic resize
          s = easeOut((f.rating-this.min)*this.mult, this.min, this.max, this.diff);
          //log('add' +u.type +' ' +u.title +', rating=' +f.rating +', s=' +s);
        }
        if(s<minNodesize)   // ensure a minimal size
          s = minNodesize;
        //s=28;  // debug fixed size
        f.width = toGridX(s);
        f.height = toGridY(s);
        f.init(this);
        addNode(f.id, f);
        this.addChild(f);
        return f;
    }
    
    this.forceLoad = function(id, refUsr){
        if(this.hasID(id))
            return;
        this.ids.push(id);
        log('apps.forceLoad id=' +id);
        /*var args = {};
        var hash;
        var domain_name = domainName;
        var nonce = randomi(23,999999);
        var domain_time_stamp =  Math.round(new Date().getTime() / 1000);
        var sessid = "7af6687c1c89a47a3d59f98e2de07dff";
        var method = "communiverse.getapp";
        var api_key = "0faa196d269711df46fc0eea2c301c58";
        var url = httpRoot+"utils/hashHmac.php?dts="+domain_time_stamp+"&dn="+domain_name+"&nonce="+nonce+"&method="+method+"&api_key="+api_key;*/
        var method = "communinode/" . id;    // get one node
        var url = apiUrl +method;  // TODO: authentication

        var nws = this;
        $.get(url, function(data){
                    var f = nws.addApp(data);
                    if(nws.forceFitField(f)){
                        if(refUsr != undefined){
                            refUsr.startTraveller(f);
                        }
                    }else{
                        
                    }
         }, 'json');
        
    }
    
    this.fillDummyData = function(){
        this.children = [];
        for(var i=0;i<30;i++){
                var f = new App();
                f.points = randomi(1, 300);
                f.name = "some app";
                f.id = nextFieldID();
                var s = randomi(30, 70);
                f.width = toGridX(s);
                f.height = toGridY(s);
                f.init(this);
                this.addChild(f);
        }
        this.processChildren();
    }
}

Apps.prototype = new Area();
