/*
 DISPLAY AND RETRIEVE ideas
*/

function Ideas(){
    this.cssClass = "ideas";
    this.name = "Ideas";
    
    this.load = function(){
        this.children = [];
        /*var hash;
        var domain_name = domainName;
        var nonce = randomi(23,999999);
        var domain_time_stamp =  Math.round(new Date().getTime() / 1000);
        var sessid = "7af6687c1c89a47a3d59f98e2de07dff";
        var method = "communiverse.getideaslist";
        var api_key = "0faa196d269711df46fc0eea2c301c58";
        var args = {};
        var url = httpRoot+"utils/hashHmac.php?dts="+domain_time_stamp+"&dn="+domain_name+"&nonce="+nonce+"&method="+method+"&api_key="+api_key;*/
        var ntype  = "idea";
        var method = "communinode?limit=" +maxFields + "&ntype=" +ntype;    // get nodes

        var url = apiUrl +method;  // TODO: authentication
        var nws = this;
        //log('ideas.load url=' + url);
        $.get(url, function(data){
                    nws.min = data["info"]["minPoints"];
                    nws.max = data["info"]["maxPoints"];
                    nws.diff = nws.max-nws.min;
                    nws.mult = 70/nws.max;
                    nws.min = nws.min*nws.mult;
                    nws.max = nws.max*nws.mult;
                    nws.diff = nws.diff*nws.mult;

                    for(var i in data["nodes"]){
                        var s = defNodesize;     // default size of Node
                        var u = data["nodes"][i];
                        var f = new Idea();

                        f.rating = u.rating;
                        f.developer = u.author;
                        f.name = u.title;
                        f.date = new Date(u.release_date*1000);
                        f.id = u.id;
                        if (f.rating > 0) {  // if there is a rating, dynamic resize
                          s = easeOut((f.rating-nws.min)*nws.mult, nws.min, nws.max, nws.diff);
                          //log('add' +u.type +' ' +u.title +', rating=' +f.rating +', s=' +s);
                        }
                        if(s<minNodesize)   // ensure a minimal size
                          s = minNodesize;
                        //s=28;  // debug fixed size
                        f.width = toGridX(s);
                        f.height = toGridY(s);
                        f.init(nws);
                        nws.addChild(f);
                    }
                    nws.draw();
                    nws.processChildren();
         }, 'json');
    }
    
    this.fillDummyData = function(){
        this.children = [];
        for(var i=0;i<30;i++){
                var f = new Idea();
                f.points = randomi(1, 300);
                f.name = "Some idea";
                f.id = nextFieldID();
                var s = randomi(30, 70);
                f.width = toGridX(s);
                f.height = toGridY(s);
                f.init(this);
                this.addChild(f);
        }
        this.processChildren();
        this.fadeInAllChildren();
    }
}

Ideas.prototype = new Area();
