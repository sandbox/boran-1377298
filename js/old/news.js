/**
 * DISPLAY AND RETREIVE News items
 */

function News(){
    this.cssClass = "news";
    this.name = "News";
    
    this.load = function(){
        this.children = [];
        //var args = {};
        //var hash;
        //var domain_name = domainName;
        //var nonce = randomi(23,999999);
        //var domain_time_stamp =  Math.round(new Date().getTime() / 1000);
        //var sessid = "7af6687c1c89a47a3d59f98e2de07dff";
        //var method = "communiverse.getnewslist";
        //var api_key = "0faa196d269711df46fc0eea2c301c58";
        //var url = httpRoot+"utils/hashHmac.php?dts="+domain_time_stamp+"&dn="+domain_name+"&nonce="+nonce+"&method="+method+"&api_key="+api_key;
        var ntype  = "news"
        var method = "communinode?limit=" +maxFields + "&ntype=" +ntype;    // get nodes

        var url = apiUrl +method;  // TODO: authentication
        var nws = this;
        //log('news.load url=' + url);
        $.get(url, function(data) {
          //log(print_r(data));
                //if (status == false) {
                //    log('error loading news');
                //} else {
                    nws.min = data["info"]["minPoints"];
                    nws.max = data["info"]["maxPoints"];
                    nws.diff = nws.max-nws.min;
                    nws.mult = 70/nws.max;
                    nws.min = nws.min*nws.mult;
                    nws.max = nws.max*nws.mult;
                    nws.diff = nws.diff*nws.mult;
                    for(var i in data["nodes"]){
                        var u = data["nodes"][i];

                        nws.ids.push(u.id);
              		//log('news.load add: ' +u.title);
			//log(print_r(u));
                        nws.addNews(u);
                    }
                    nws.draw();
                    nws.processChildren();
         }, 'json');
    }
    
    this.addNews = function(u){
        var f = new NewsEntry();
        var s = defNodesize;     // default size of Node
        f.rating = u.rating;
        f.developer = u.author;
        f.name = u.title;
        f.date = new Date(u.release_date*1000);
        f.id = u.id;
        if (f.rating > 0) {  // if there is a rating, dynamic resize
          s = easeOut((f.rating-this.min)*this.mult, this.min, this.max, this.diff);
          //log('add' +u.type +' ' +u.title +', rating=' +f.rating +', s=' +s);
          if(s<minNodesize)   // ensure a minimal size
            s = minNodesize;
        }
        f.width = toGridX(s);
        f.height = toGridY(s);
        f.init(this);
        addNode(f.id, f);
        this.addChild(f);
        return f;
    }
    
    this.forceLoad = function(id, refUsr){
        if(this.hasID(id))
            return;
        this.ids.push(id);
        //olog('news.forceLoad id=' +id);
        /*var hash;
        var domain_name = domainName;
        var nonce = randomi(23,999999);
        var domain_time_stamp =  Math.round(new Date().getTime() / 1000);
        var sessid = "7af6687c1c89a47a3d59f98e2de07dff";
        var method = "communiverse.getnews";
        var api_key = "0faa196d269711df46fc0eea2c301c58";
        var args = {};
        var url = httpRoot+"utils/hashHmac.php?dts="+domain_time_stamp+"&dn="+domain_name+"&nonce="+nonce+"&method="+method+"&api_key="+api_key;*/
        var method = "communinode/" + id;     // get one node
        var url = apiUrl +method;  // TODO: authentication

        var nws = this;
        $.get(url, function(data){
                    var f = nws.addNews(data);
                    if(nws.forceFitField(f)){
                        if(refUsr != undefined){
                            refUsr.startTraveller(f);
                        }
                    }else{
                        
                    }
         }, 'json');
    }
    
    this.fillDummyData = function(){
        this.children = [];
        for(var i=0;i<30;i++){
                var f = new NewsEntry();
                f.points = randomi(1, 300);
                f.name = "News entry";
                f.id = nextFieldID();
                var s = randomi(30, 70);
                f.width = toGridX(s);
                f.height = toGridY(s);
                f.init(this);
                this.addChild(f);
        }
        this.processChildren();
    }
}

News.prototype = new Area();
