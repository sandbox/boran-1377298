function NewsEntry(){
    this.developer = "swisscom";
    this.date = 0;
    this.cssClass = "newsEntry";
    this.area  = "n";
    this.rating = 0;
    this.link;
    
    this.getInfoData = function(){
	    // "Poster: "+this.developer,
      return ["Category: News", "Date: "+(this.date.getDay()+1)+"."+(this.date.getMonth()+1)+"."+this.date.getFullYear()];
    }
}

NewsEntry.prototype = new Field();
