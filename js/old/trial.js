function Trial(){
    this.launchDate = new Date(randomi(2006, 2012), randomi(1,12), randomi(1,30));  // TOOD: er, why random!!
    this.cssClass = "trial";
    this.area  = "t";
    
    this.getInfoData = function(){
	    return ["Category: Trial", "Launch: "+(this.launchDate.getDay()+1)+"."+(this.launchDate.getMonth()+1)+"."+this.launchDate.getFullYear()];
    }
}

Trial.prototype = new Field();
