/*
 DISPLAY AND RETRIEVE trials
*/

function Trials(){
    this.cssClass = "trials";
    this.name = "Trials";
    
    this.load = function(){
        this.children = [];
        /*var hash;
        var domain_name = domainName;
        var nonce = randomi(23,999999);
        var domain_time_stamp =  Math.round(new Date().getTime() / 1000);
        var sessid = "7af6687c1c89a47a3d59f98e2de07dff";
        var method = "communiverse.gettrialslist";
        var api_key = "0faa196d269711df46fc0eea2c301c58";
        var args = {};
        var url = httpRoot+"utils/hashHmac.php?dts="+domain_time_stamp+"&dn="+domain_name+"&nonce="+nonce+"&method="+method+"&api_key="+api_key;*/
        var ntype  = "idea";
        //var ntype  = "trialgroup";
        var method = "communinode?limit=" +maxFields + "&ntype=" +ntype;    // get nodes

        var url = apiUrl +method;  // TODO: authentication
        var nws = this;
        log('trials.load url=' + url);
        $.get(url, function(data){
                    nws.min = data["info"]["minPoints"];
                    nws.max = data["info"]["maxPoints"];
                    nws.diff = nws.max-nws.min;
                    nws.mult = 70/nws.max;
                    nws.min = nws.min*nws.mult;
                    nws.max = nws.max*nws.mult;
                    nws.diff = nws.diff*nws.mult;
                    //for(var i in data["trials"]){
                    //    var u = data["trials"][i];
                    for(var i in data["nodes"]){
                        var u = data["nodes"][i];

                        var f = new Trial();
                        f.developer = u.author;
                        f.name = u.title;
                        f.date = new Date(u.release_date*1000);
                        f.id = u.id;
                        s = 40;    // TODO
                        //var s = easeOut((f.rating-nws.min)*nws.mult, nws.min, nws.max, nws.diff);
                        f.width = toGridX(s);
                        f.height = toGridY(s);
                        f.init(nws);
                        nws.addChild(f);
                    }
                    nws.draw();
                    nws.processChildren();
         }, 'json');
    }
    
    this.fillDummyData = function(){
        this.children = [];
        for(var i=0;i<30;i++){
                var f = new Trial();
                f.points = randomi(1, 300);
                f.name = "Some trial";
                f.id = nextFieldID();
                var s = randomi(30, 70);
                f.width = toGridX(s);
                f.height = toGridY(s);
                f.init(this);
                this.addChild(f);
        }
        this.processChildren();
        this.fadeInAllChildren();
    }
}

Trials.prototype = new Area();
