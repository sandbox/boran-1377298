/*
 *
 *  toggle to hide the content and the sidebar: i.e. put the communitree in the fore or background
 *  TODO: verfy spefic to one one theme where specific regions has to be hidden or not
 */
var contentIsShown = "yes";

function showCommunitree() {  
    document.getElementById("cTreeRoot").style.zIndex = "10000";
    document.getElementById("cTreeRoot").style.display = "block";
    if (document.getElementById("zone-postscript-wrapper") != null)
      document.getElementById.style.display = "none";
    if (document.getElementById("region-sidebar-second") != null)
      document.getElementById("region-sidebar-second").style.display = "none";
    if (document.getElementById("zone-footer-wrapper") != null)
      document.getElementById("zone-footer-wrapper").style.display = "none";

    $("#cTreeToogleButton").addClass("closeButton");
		contentIsShown = "no";
}

function hideCommunitree() {
    document.getElementById("cTreeRoot").style.display = "none";
    document.getElementById("cTreeRoot").style.zIndex = "-10000";
    if (document.getElementById("zone-postscript-wrapper") != null)
      document.getElementById("zone-postscript-wrapper").style.display = "block";
    if (document.getElementById("region-sidebar-second") != null)
      document.getElementById("region-sidebar-second").style.display = "block";
    if (document.getElementById("zone-footer-wrapper") != null)
      document.getElementById("zone-footer-wrapper").style.display = "block";

    $("#cTreeToogleButton").removeClass("closeButton");
		contentIsShown = "yes";
}

function toggleTree(){
  if (contentIsShown == "yes") {
    showCommunitree();  
  }
  else {
    hideCommunitree();
  }
}

    //Code of Luca that shows the content and the sidebar and hide the "?" box
/*
		document.getElementById('zone-user-wrapper').style.display = 'block';	
		document.getElementById('zone-header-wrapper').style.display = 'block';	
		document.getElementById('zone-preface-wrapper').style.display = 'block';	
		document.getElementById('region-inner-content').style.display = 'block';	
		document.getElementById('region-sidebar-second').style.display = 'block';	
		document.getElementById('zone-postscript-wrapper').style.display = 'block';	
		document.getElementById('zone-footer-wrapper').style.display = 'block';	
 		document.getElementById('cTreeRoot').style.opacity = "0.3";	
*/
    //? box is hidden
/*
		document.getElementById('fieldhelp-10').style.display = 'none';	
    document.getElementById("sizer").style.height = "0px";
*/

/*
		document.getElementById('zone-user-wrapper').style.display = 'none';	
		document.getElementById('zone-header-wrapper').style.display = 'none';	
		document.getElementById('zone-preface-wrapper').style.display = 'none';	
*/
/* 		document.getElementById('breadcrumb').style.display = 'none';	 */
/*
		document.getElementById('region-inner-content').style.display = 'none';	
		document.getElementById('region-sidebar-second').style.display = 'none';	
		document.getElementById('zone-postscript-wrapper').style.display = 'none';	
		document.getElementById('zone-footer-wrapper').style.display = 'none';	
 		document.getElementById('cTreeRoot').style.opacity = "1";	
*/
    //? box is shown
/*
		document.getElementById('fieldhelp-10').style.display = 'block';	
    document.getElementById("sizer").style.height = "700px";
*/
