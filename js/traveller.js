var travellers = [];

function killTravellers(){
    for(var i = 0; i<travellers.length; i++){
	travellers[i].killFade();
    }
    travellers = [];
}

function Traveller(){
    var connection = null;
    var pos = 0;
    var interpol = 0;
    
    this.element = null;
    
    this.width = 9;
    this.height = 9;
    
    this.widthHalf = 4;
    this.heightHalf = 4;
    
    this.parent = lineElement;
    this.id = nextTravellerID();
    
    this.parent.append("<div id='traveller"+this.id+"' class='traveller'>&nbsp;</div>");
    this.element = $("#traveller"+this.id);
    
    this.element.width(this.width);
    this.element.height(this.height);
    
    this.fieldA = null;
    this.fieldB = null;
        
    this.speed = .55;
    
    this.intervalID=0;
    
    this.dir = -1;
    this.oldDir = -1;
    
    travellers.push(this);
    
    this.setConnection = function(con){
        this.connection = con;
        
	this.go();
    }
    
    this.setFields = function(a, b){
	if(a == undefined || b == undefined)
	    return;
        this.fieldA = a;
        this.fieldB = b;
	
        a.activate();
	//b.activate();
	var con = new Connection();
		//con.addPassThru(a);
		//con.addPassThru(b);
	var aStart = a.getConnectionStart(b);
	var bStart = b.getConnectionStart(a);
	con.init(aStart, bStart);
	con.addPointBeginning(a.getCenter());
	con.addPointEnd(b.getCenter());
	//con.draw();
        this.setConnection(con);
    }
    
    this.getDirection = function(a, b){
        this.oldDir = this.dir;
        this.dir = getDirection(a, b);
        this.updateBG();
    }
    
    this.go = function(){
        var theRef = this;
	var speed = 30;
	if($.browser.msie == true){
	    speed = 30;
	}
        this.intervalID = setInterval(function(){theRef.process()}, speed);
        this.pos = 1;
        this.interpol = 0;
        this.getDirection(this.connection.pointAt(0), this.connection.pointAt(1));
    }
    
    this.process = function(){
        var a = this.connection.pointAt(this.pos);
        var b = this.connection.pointAt(this.pos-1);
        this.x = a.x;
        this.y = b.y;
        this.draw();
        
        this.getDirection(b, a);
        
        this.interpol+=this.speed;
        if(this.interpol>1){
            this.connection.drawStep(this.pos);
	    
	    this.pos++;
            this.interpol = this.interpol-1;
	    
	    if(this.pos == this.connection.pointsAmount()-3)
		this.fieldB.fadeIn();
	    
	    if(this.pos == this.connection.pointsAmount()){
                clearInterval(this.intervalID);
		this.killArrowFade();
            }
            
        }
    }
    
    this.killFade = function(){
	var fadeSpeed = 900;
	var ref = this;
	this.element.fadeOut(fadeSpeed);
	if(this.connection != undefined)
	    this.connection.element.fadeOut(fadeSpeed, function(){ref.kill()});

    }
    
    this.kill = function(){
	
        this.fieldA.deactivate();
        this.fieldB.deactivate();
        this.connection.kill();
    }
    
    this.killArrowFade = function(){
	var ref = this;
	this.element.fadeOut(300, function(){ref.killArrow()});
    }
    
    this.killArrow = function(){
	this.element.html("");
        this.element.css("visibility","hidden");
    }
    
    this.draw = function(){
        this.element.css("left", this.x-this.widthHalf);
        this.element.css("top", this.y-this.heightHalf);
    }
    
    this.updateBG = function(){
	if($.browser.msie == true){
		return;
	}
        if(this.oldDir == this.dir)
            return;
        if(this.dir == UP)
            this.element.css("background", "url("+httpRoot+"images/traveller/animArrowT.gif)");
         if(this.dir == DOWN)
            this.element.css("background", "url("+httpRoot+"images/traveller/animArrowB.gif)");
         if(this.dir == RIGHT)
            this.element.css("background", "url("+httpRoot+"images/traveller/animArrowR.gif)");
         if(this.dir == LEFT)
            this.element.css("background", "url("+httpRoot+"images/traveller/animArrowL.gif)");
    }
}

Traveller.prototype = new Rect(0,0,0,0);
