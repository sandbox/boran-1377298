function User(){
    this.points = 0;
    this.cssClass = "user";
    this.joinDate;
    this.image;
    this.area  = "u";
    this.id = 0;
    this.blocked = false;
    this.reference = this;
    
    this.getInfoData = function(){
	return [
	  "Points: "+this.points, 
	  "Registered: "+(this.joinDate.getDate())+"."+(this.joinDate.getMonth()+1)+"."+this.joinDate.getFullYear()
	  //TODO: achievements, number of ideas, comments, likes?
        ];
    }
    
    this.getImageData = function(){
	return this.image;
    }
    
    this.onInit = function(){
	var ref = this;
	this.element.dblclick(function(){ref.onDblClick()});
    }
    
    /* 
     * On clicking a user, jump to their profile page
     */
    this.onClick = function(){
    //this.onDblClick = function(){
	//var target = "user/"+this.id;     // does not work?
	var target = httpServer +"user/" +this.id;     
	log('user.onDblClick target=' +target);
	if($.browser.msie == true){
		$(document)[0].location = target;
		return;
	}
	//master.showContent();
	//$(document)[0].location.hash = target;    // TODO does not work with index.html?
	$(document)[0].location = target;
	//$('#content-inner').html("<div style='width: 100%; text-align:left;margin-top: 50px'><img src='"+httpServer+"sites/all/modules/ctools/images/status-active.gif' /> loading...</div>");
	//$('#content-inner').load(httpServer+target+"?noHeadFoot");
    }

/*  
    this.onClick = function(){
  	toggleTree();//from toggle.js by LS
	if(this.blocked)
	    return;
	this.blocked = true;
    }*/
    

  /*
   * Draw lines from the user objects to nodes that the user was active on
   * 8.3.12: moved the activity drawing from the Click to a separate function which is called by 
   * the parent field object on hovering.
   **/
   this.drawActivity = function(){
 	//master.hideContent(); 
	killTravellers();
	/*var hash;
        var domain_name = domainName;
        var nonce = randomi(23,999999);
        var domain_time_stamp =  Math.round(new Date().getTime() / 1000);
        var sessid = "7af6687c1c89a47a3d59f98e2de07dff";
        var method = "communiverse.useractivity";
        var api_key = "0faa196d269711df46fc0eea2c301c58";*/
	var limit = 13;
	if($.browser.msie == true){
	    limit = 3;
	}
        var args = {uid:this.id, limit:limit};
	var ref = this;
	
        //var url = httpRoot+"utils/hashHmac.php?dts="+domain_time_stamp+"&dn="+domain_name+"&nonce="+nonce+"&method="+method+"&api_key="+api_key;
        var method = "communiactivity/" + this.id + "?limit=" + limit;    // call useractivity
        var url = apiUrl +method;  // TODO: authentication

        $.get(url, function(data){
 		//log('user.drawActivity ' +url);
		ref.blocked = false;
                    for(var i in data){
			var doTrav = true;
                        var n = data[i];
			var f = undefined;
			var area = undefined;
			if(n.entitytype == "node"){
			    area = master.nodes;
			}	
			else if(n.entitytype == "user")
			    area = master.users;
			if(area != undefined){ 
			    f = area.getVisibleByID(n.entityid);
			    if(f == undefined){
			        f = area.getByID(n.entityid);
			        if(f == undefined){
				    //log('area ' +area +' has no ID, forceload');
				    area.forceLoad(n.entityid, ref);   // TODO: why is this needed? it generates lots of queries
			        }else{
			            doTrav = area.forceFitField(f);
			        }
			    }
			}
			if(doTrav){
			    ref.startTraveller(f);
			}
		    }
        }, 'json');
    }
    
    this.startTraveller = function(f){
	var trav = new Traveller();
	    if(f != undefined)
		trav.setFields(this, f);
    }
    
    this.setAsTheUser = function(){
	this.element.addClass("theUser");
	this.defaultBG = httpRoot+"images/php/roundCorner.php?scale="+this.width+"&style=red&area="+this.area;
	this.hoverBG = this.defaultBG;
	this.activeBG = this.defaultBG;
	if($.browser.msie == true)
	    this.element.css('filter', "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+this.defaultBG+"', sizingMethod='scale')");
	else
	    this.element.css("background-image", "url("+this.defaultBG+")");
    }
}

User.prototype = new Field();
