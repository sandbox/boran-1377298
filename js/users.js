/*
 DISPLAY AND RETREIVE USERS
*/

function Users(){
    this.cssClass = "users";
    this.name = "Users";
    this.idToUser = new Object();
    this.activeUser = undefined;
    
    this.load = function(){
        this.children = [];
        //var args = {};
        //var method = "communiverse.getusers";
        //var domain_name = domainName;
        //var nonce = randomi(23,999999);
        //var domain_time_stamp =  Math.round(new Date().getTime() / 1000);
        //var sessid = "7af6687c1c89a47a3d59f98e2de07dff";
        //var hash;
        //var api_key = "0faa196d269711df46fc0eea2c301c58";
        //var url = httpRoot+"utils/hashHmac.php?dts="+domain_time_stamp+"&dn="+domain_name+"&nonce="+nonce+"&method="+method+"&api_key="+api_key;
        var method = "communiuser?limit="+maxFields;    // index max XX users with most points
        var url = apiUrl +method;  // TODO: authentication

        var usrs = this;
	      log('load=' + url);
        $.get(url, function(data){                // Jquery get url return data to this function
          //log(print_r(data));
          usrs.min = data["info"]["minPoints"];
          usrs.max = data["info"]["maxPoints"];
          usrs.diff = usrs.max-usrs.min;
          usrs.mult = 110/usrs.max;
          usrs.min = usrs.min*usrs.mult;
          usrs.max = usrs.max*usrs.mult;
          usrs.diff = usrs.diff*usrs.mult;
            for(var i in data["users"]){
              var u = data["users"][i];
              usrs.addUser(u);
              //log('users.load add user: ' +u.username);
	            //log(print_r(u));
            }
                    usrs.processChildren();
                    usrs.draw();
                    if(usrs.activeUser !=0){
                        var f = usrs.getVisibleByID(usrs.activeUser);
                        if(f == undefined){
                            f = usrs.getByID(usrs.activeUser);
                            if(f != undefined)
                                usrs.forceFitField(f);
                            else{
                                usrs.loadUser(usrs.activeUser);
                            }
                        }
                        if(f != undefined)
                            f.setAsTheUser();
                    }

        }, 'json');
        
/*
	$.get(url,
        function(data){
            hash = data;
            params = {hash: hash, nonce: nonce, domain_name: domain_name, domain_time_stamp: domain_time_stamp, sessid: sessid};
            var args = {"limit":maxFields};
            params = mergeJSON(params, args);
            Drupal.service(method, params, function(status, data) {
                if(status == false) {
                    //alert('Error loading user history');
                } else {
                }
            });
         });
*/
    }

    this.addUser = function(u){
        var f = new User();
	//log('addUser ' +u.username);
        f.points = u.points;
        f.name = u.username;
        if(u.picture != "")
            f.image = u.picture;
            //f.image = httpServer +'/' +u.picture;
        f.joinDate = new Date(u.joined*1000);
        f.id = u.id;
        var s = easeOut((f.points-this.min)*this.mult, this.min, this.max, this.diff);
        if(s<14)
            s = 14;
        f.width = toGridX(s);
        f.height = toGridY(s);
        f.init(this);
        addNode(f.id, f);
        this.addChild(f);
        this.idToUser[f.id] = f;
        return f;
    }
    

    this.loadUser = function(id){
	log('loadUser id=' +id);
        /*var hash;
        var domain_name = domainName;
        var nonce = randomi(23,999999);
        var domain_time_stamp =  Math.round(new Date().getTime() / 1000);
        var sessid = "7af6687c1c89a47a3d59f98e2de07dff";
        var method = "communiverse.getuser";
        var api_key = "0faa196d269711df46fc0eea2c301c58";
        var url = httpRoot+"utils/hashHmac.php?dts="+domain_time_stamp+"&dn="+domain_name+"&nonce="+nonce+"&method="+method+"&api_key="+api_key;*/
        var method = "communiuser/" . id;    // get one user
        var url = apiUrl +method;  // TODO: authentication

        var usrs = this;
        $.get(url, function(data){
                    var f = usrs.addUser(data);
                    usrs.forceFitField(f);
                    if(data["id"] == usrs.activeUser)
                        f.setAsTheUser();
        }, 'json');
    }
    
    this.setActiveUser = function(userID){
        this.activeUser = userID;
    }
    
    this.fillDummyData = function(){
        this.children = [];
        var names = ["Niklaus", "Martin", "Philip", "undef", "Konrad", "Raphael", "Sean", "Tobi", "Yummy Industries", "This could be you"];
        for(var i=0;i<250;i++){
                var f = new User();
                f.points = randomi(1, 300);
                f.name = names[randomi(0, names.length-1)];
                f.id = nextFieldID();
                var s = f.points/300*60;
                if(s<14)
                    s = 14;
                f.width = toGridX(s);
                f.height = toGridY(s);
                f.init(this);
                this.addChild(f);
        }
        this.processChildren();
        //log(this.childrenFit[10].name);
        this.childrenFit[10].setAsTheUser();
        this.childrenFit[10].name = "This is you";
    }
}

Users.prototype = new Area();

