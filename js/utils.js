/*
* CONTAINS ALL SORT OF HELPER FUNCTIONS AND CLASSES
*/

function map( value,  istart,  istop,  ostart,  ostop) {
	return ostart + (ostop - ostart) * ((value - istart) / (istop - istart));
}

//random float in range, where a < b
function randomf(a, b){
	var d = b-a;
	return Math.random()*d+a;
}

//random int in range, where a < b
function randomi(a,b){
	return Math.floor(randomf(a,b+1));
}

function random1i(){
	if(Math.random()>.5)
		return -1;
	else
		return 1;
}

var gridSizeX = 7;
var gridSizeY = gridSizeX;
//convert a float value to fit into the grid
function toGridX(a){
	return Math.floor(a/gridSizeX)*gridSizeX;
}

function toGridY(a){
	return toGridX(a);
}

var fieldID = 99999;

function nextFieldID(){
	return fieldID++;
}

var lineID = 0;

function nextLineID(){
	return lineID++;
}

var travellerID = 0;

function nextTravellerID(){
	return travellerID++;
}

var connectionID = 0;

function nextConnectionID(){
	return connectionID++;
}

var areaID = 0;

function nextAreaID(){
	return areaID++;
}

function isInRect(x, y, rx, ry, w, h){
	if(x<=rx)return false;
	if(x>=rx+w)return false;
	if(y<=ry)return false;
	if(y>=ry+h) return false;
	return true;
}

//a class representing a point
function Point(_x, _y){
	this.x = _x;
	this.y = _y;
	
	this.toJsPoint = function(){
		return new jsPoint(this.x, this.y);
	}
}

//a class representing a rectangle
function Rect(_x, _y, _w, _h){
	this.x = _x;
	this.y = _y;
	this.width = _w;
	this.height = _h;
	
	this.set = function(x, y, w, h){
		this.x = x;
		this.y = y;
		this.width = w;
		this.height = h;
	}
	
	this.intersects = function(r){
		if(this.bottom() <= r.top())		return false;
		if(this.top() >= r.bottom())		return false;
		if(this.right() <= r.left())		return false;
		if(this.left() >= r.right())		return false;
		return true;
	};
	
	this.intersectsWithSpace = function(r){
		if(this.bottom()+gridSizeY <= r.top())	return false;
		if(this.top()-gridSizeY >= r.bottom())	return false;
		if(this.right()+gridSizeX <= r.left())	return false;
		if(this.left()-gridSizeX >= r.right())	return false;
		return true;
	};
	
	this.contains = function(x, y){
		return isInRect(x,y,this.x,this.y,this.width,this.height);
	}
	
	this.top = function(){
		return this.y;
	};
	
	this.bottom = function(){
		return this.y+this.height;
	};
	
	this.left = function(){
		return this.x;
	}
	
	this.right = function(){
		return this.x+this.width;
	}
	
	this.centerXf = function(){
		return this.x+this.width*.5;
	}
	
	this.centerYf = function(){
		return this.y+this.height*.5;
	}
	
	this.centerXi = function(){
		return this.x+Math.round(this.width*.5);
	}
	
	this.centerYi = function(){
		return this.y+Math.round(this.height*.5);
	}
	
	this.centeri = function(){
		return new Point(this.centerXi(), this.centerYi());
	}

	this.centerf = function(){
		return new Point(this.centerXf(), this.centerYf());
	}
}

function linkDescr(title, url){
	this.title = title;
	this.url = url;
}

function easeOut (t, b, c, d) {
	return -c * ((t=t/d-1)*t*t*t - 1) + b;
}

function mergeJSON(obja, objb) {
  var i = 0;
  for (var z in objb) {
    if (objb.hasOwnProperty(z)) {
      obja[z] = objb[z];
    }
  }
  return obja;
}

//ported print_r from javascript 
function print_r(theObj,indent){
      var output="";
      if (indent == undefined) { indent = "  "; } else { indent += "  "; }
      if(theObj.constructor == Array || theObj.constructor == Object) {
        for(var p in theObj){
          if(theObj[p].constructor == Array|| theObj[p].constructor == Object){
              var type = (theObj[p].constructor == Array) ? "Array" : "Object";
              output += indent+"["+p+"]("+type+")=>\n";
              output += print_r(theObj[p],indent);
          } else { output += indent+"["+p+"]:"+theObj[p]+"\n"; }
        }
      }
	return output;
}

function deselect() 
{
   if (document.selection)
             document.selection.empty();
   else if (window.getSelection)
              window.getSelection().removeAllRanges();
} 