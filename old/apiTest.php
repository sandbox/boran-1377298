<?php
// http://sislabsx.vptt.ch/sites/all/themes/sclabs4/cTree3/apiTest.php

$domain_name = "sislabsx.vptt.ch";
$nonce = rand(2222,999999);
$domain_time_stamp =  time();
$sessid = "7af6687c1c89a47a3d59f98e2de07dff";
$method = "communiverse.getusers";
$api_key = "0faa196d269711df46fc0eea2c301c58";
$hash =  hash_hmac('sha256', $domain_time_stamp.';'.$domain_name.';'.$nonce.';'.$method, $api_key);
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
    "http://www.w3.org/TR/html4/strict.dtd"
    >
<html lang="en">
<head>
    <title>Communitree sislabsx.vptt.ch API test</title>
    <script type='text/javascript' src="js/jquery.js"></script>
    <script src="../../../../../../../misc/drupal.js" type="text/javascript"></script>
    <script src="js/json_server.js" type="text/javascript"></script> 
    <script type='text/javascript' src="js/utils.js"></script>
    <script type='text/javascript' src="js/logger.js"></script>
    <script type="text/javascript">
        var logger;
        
        //setTimeout("location.href = 'http://sislabsx02.vptt.ch/sites/all/themes/sclabs_dev/cTree2/apiTest.html';",4000);
        
        $(document).ready(function(){
            logger = new Logger();
            logger.init();
            logger.toggle();
            
            var domain_name = "sislabsx.vptt.ch";
            var nonce = randomi(23,999999);
            var domain_time_stamp =  Math.round(new Date().getTime() / 1000);;
            //var method = "communiverse.getusers";
            var method = "api/communiuser/1";      // ***********************
            var sessid = "7af6687c1c89a47a3d59f98e2de07dff";
            var api_key = "0faa196d269711df46fc0eea2c301c58";
            
            var hash;
            var url = "utils/hashHmac.php?dts="+domain_time_stamp+"&dn="+domain_name+"&nonce="+nonce+"&method="+method+"&api_key="+api_key;
            $.get(url,
            function(data){
                hash = data;
                logger.log("CALLING API");
                logger.log("DOMAIN:  "+domain_name);
                logger.log("NONCE:  "+nonce);
                logger.log("SESSID:  "+sessid);
                logger.log("METHOD:  "+method);
                logger.log("TIME STAMP:  "+domain_time_stamp);
                logger.log("API KEY:  "+api_key);
                logger.log("HASH:  "+hash);
                logger.log("URL "+url);
                var args = {};
                params = {hash: hash, nonce: nonce, domain_name: domain_name, domain_time_stamp: domain_time_stamp, sessid: sessid}
                params = mergeJSON(params, args);
                Drupal.service(method, params, function(status, data) {
                    if(status == false) {
                        alert('Error');
                    }else{
                        alert('OK');
                        logger.log("JS got <pre>"+print_r(data)+"</pre>");
                    }
                });
            });
        })
    </script>
</head>
<body>

<?php
echo $domain_name . ", hash=$hash";
echo '
    <script type="text/javascript">
    $.ajax({
                    type: "GET",
                    //type: "POST",
                    //url: "http://'.$domain_name.'/services/json",
                    //url: "http://'.$domain_name.'/api",
                    url: "http://'.$domain_name.'/api/communiuser/1",
                    data: "hash='.$hash.'&domain_name='.$domain_name.'&nonce='.$nonce.'&domain_time_stamp='.$domain_time_stamp.'&sessid='.$sessid.'&method='.$method.'",
                    dataType: "json",
                    success: function(data){
                        logger.log("PHP got "+print_r(data));
                    }
                });
    </script>
'
?>
</body>
</html>
