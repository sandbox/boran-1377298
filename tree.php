<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"> 
<head> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
	<title>labs communitree</title> 
	<link rel="SHORTCUT ICON" href="/undeficon.png" /> 
	<link href="style/ctree.css" rel="stylesheet" type="text/css" media="all" /> 

	<script src="../../../../../../../misc/jquery.js" type="text/javascript"></script>

	<script type='text/javascript' src="js/logger.js"></script>
	<script type='text/javascript' src="js/toggle.js"></script>
	<script type='text/javascript' src="js/utils.js"></script>
	<script type='text/javascript' src="js/clickMe.js"></script>
	<script type='text/javascript' src="js/md5.js"></script>
	<script type='text/javascript' src="js/distributor.js"></script>
	<script type='text/javascript' src="js/area.js"></script>
	<script type='text/javascript' src="js/field.js"></script>
	<script type='text/javascript' src="js/connection.js"></script>
	<script type='text/javascript' src="js/traveller.js"></script>
	<script type='text/javascript' src="js/hoverInfo.js"></script>
	<script type='text/javascript' src="js/users.js"></script>
	<script type='text/javascript' src="js/user.js"></script>
	<script type='text/javascript' src="js/nodes.js"></script>
	<script type='text/javascript' src="js/node.js"></script>
	<script type='text/javascript' src="js/spacerField.js"></script>
	<script type='text/javascript' src="js/labsMaster.js"></script>
	<script type='text/javascript' src="js/config.js"></script>
  <!-- Manaul settings must come after config.js to override
	<script type='text/javascript'>var $ = jQuery.noConflict();</script>
  -->
	<script type='text/javascript'>var communitree_activeUser=1;</script>
<!-- 	<script type='text/javascript'>enableLogging = true;</script> -->
	<script type='text/javascript' src="js/main.js"></script>

<!--
-->
</head> 
<body>
<?php

//Settings
$title = "Swisscom Labs";
$slogan = "Community-nurtured innovation";
$image_path = "images/logo.gif"; //relative or absolute path of your logo

print '<div id="">'
      .'<img class="ctree_logo" src='.$image_path.' />'
      .'<h1 class="ctree_title">'.$title.'</h1>'
      .'<h2 class="ctree_slogan">'.$slogan.'</h2>';

?>
<div id="cTreeRoot"></div>

</body> 
</html>
